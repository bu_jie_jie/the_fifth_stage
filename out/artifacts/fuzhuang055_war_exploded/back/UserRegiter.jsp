<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/16
  Time: 15:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>注册</title>
    <link rel="stylesheet" href="../css/back/UserRegister.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/back/UserRegister.js"></script>
</head>
<body>
<div>
    <div class="biaodan">
<%--        <form action="/UserRegiter" method="post">--%>
            <div class="lin">
                <div class="title">用户名：</div>
                <div class="shuru"><input type="text" id="username" name="username" onblur="checkUsername()"></div>
                <label id="usernameTip"></label>
            </div>
            <div class="lin">
                <div class="title">密码：</div>
                <div class="shuru"><input type="text" id="password" name="password" pattern="^[a-zA-Z]\w{5,15}$"></div>
            </div>
            <div class="lin">
                <div class="title">性别：</div>
                <div class="shuru"><input type="text" id="sex" name="sex"></div>
            </div>
            <div class="lin">
                <div class="title">生日：</div>
                <div class="shuru"><input type="text" id="birthday" name="birthday"></div>
            </div>
            <div class="lin">
                <div class="title">手机号：</div>
                <div class="shuru"><input type="text" id="phone" name="phone" onblur="checkPhone()"></div>
                <label id="phoneTip"></label>
            </div>
            <div class="linjianjie">
                <div class="title">个人简介：</div>
                <div class="shuru2"><textarea id="introduce" name="introduce"></textarea></div>
            </div>

            <div class="lin">
                <div class="title"></div>
                <div class="shuru"><input class="input3" type="button" value="提交" onclick="judge()"></div>
                <label id="tijiaoTip"></label>
            </div>
<%--        </form>--%>
    </div>
</div>
</body>
</html>
