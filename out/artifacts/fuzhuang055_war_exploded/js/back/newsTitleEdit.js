$(function () {
    getDate();
})
function getDate() {
    //获取本地的Id
    let id=localStorage.getItem("id");
    let data=myAjax("/back/newsTitleEdit/findById",{id:id},"post");
    if (data!=null){
        data=data.newsTitleModel;
        $("#title").val(data.title);
        $("#time").val(data.time);
        $("input[name=isShow][value=" + data.isShow + "]").attr("checked", true);
        $("input[name=isTop][value=" + data.isTop + "]").attr("checked", true);
    }
}


function edit(){
    let data={
        //获取session的ID
        id:sessionStorage.getItem("id"),
        title:$("#title").val(),
        time:$("#time").val(),
        isShow:$("input[name='isShow']:checked").val(),
        isTop:$("input[name='isTop']:checked").val()
    };
    let da=myAjax("/back/newsTitle/edit",data,"post");
    if (da.count==1){
        alert("编辑成功");
        $('#right').load('/back/newsTitle/newsTitleFindAll.html');
    } else {
        alert("编辑失败");
    }
}
