function save() {
    let data = {
        name: $.trim($("#navName").val()),
        src: $.trim($("#src").val()),
        creationTime: $.trim($("#creationTime").val()),
        updateTime: $.trim($("#updateTime").val()),
        isShow: $("input[name='isShow']:checked").val(),
        isTop: $("input[name='isTop']:checked").val()
    }
    let result = myAjax("/back/nav/add", data, "post")
    if (result.count == 1) {
        alert("添加成功");
        $("#right").load("/back/nav/navFindAll.html");
    } else {
        alert("添加失败");
    }
}
