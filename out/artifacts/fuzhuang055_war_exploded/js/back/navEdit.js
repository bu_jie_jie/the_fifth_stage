$(function () {
    getDate();
})
function getDate() {
    //获取本地的Id
    let id=localStorage.getItem("id");
    let data=myAjax("/back/nav/findById",{id:id},"post");
    if (data!=null){
        data=data.navModel;
        $("#navName").val(data.name);
        $("#src").val(data.src);
        $("input[name=isShow][value=" + data.isShow + "]").attr("checked", true);
        $("input[name=isTop][value=" + data.isTop + "]").attr("checked", true);
    }
}


function edit(){
    let data={
        //获取session的ID
        id:sessionStorage.getItem("id"),
        name:$("#navName").val(),
        src:$("#src").val(),
        isShow:$("input[name='isShow']:checked").val(),
        isTop:$("input[name='isTop']:checked").val()
    };
    let da=myAjax("/back/nav/edit",data,"post");
    if (da.count==1){
        alert("编辑成功");
        $('#right').load('/back/nav/navFindAll.html');
    } else {
        alert("编辑失败");
    }
}
