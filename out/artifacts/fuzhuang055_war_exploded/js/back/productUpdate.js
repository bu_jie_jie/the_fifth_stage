$(function () {
    findAll();
    findById();
})

//得到服装类别
function findAll() {
    let data = myAjax("/back/clothing/findAll", {}, "get");
    let html = '';
    console.log(data);
    data = data.list;
    if (data != null) {
        for (let i = 0; i < data.length; i++) {
            html += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
        }
    }
    $("#cloth").html(html);
}

function findById() {
    let id = localStorage.getItem("id");
    let data = myAjax("/back/product/findById", {id: id}, "get")
    setProductDate(data.productModel)
}

function setProductDate(data) {
    $("#cloth").val(data.clothingId);
    $("#cloth option[value=" + data.clothingId + "]").attr("selected", true);
    $("#productName").val(data.productName);
    $("#price").val(data.price);
    $("input[name=isShow][value=" + data.isShow + "]").attr("checked", true);
    $("input[name=isRecommend][value=" + data.isRecommend + "]").attr("checked", true);
    $("#href").attr("src", data.imgHref);
}

function save() {
    let data=new FormData();
    data.append("cloth",$("#cloth").val())
    data.append("productName",$("#productName").val());
    data.append("price",$("#price").val());
    data.append("isShow",$("input[name='isShow']:checked").val());
    data.append("isRecommend",$("input[name='isRecommend']:checked").val());
    data.append("imgHref",$("#imgHref")[0].files[0]);
    data.append("id",localStorage.getItem("id"));
    $.ajax({
        url: "/back/product/update",//链接后台的地址：对应webservlet里面的值
        data: data,//前端传递给后台的值
        type: "post",//对应servlet里面的doget或者dopost
        async: false,
        processData:false,
        contentType:false,
        dataType: 'json',//统一从后台传递给前端格式
        success: function (data) {//后台传递给前端的值
            if (data.count == 1) {
                $("#right").load("/back/product/productFindAll.html");
            } else {
                alert("编辑失败");
            }
        }
    })
}