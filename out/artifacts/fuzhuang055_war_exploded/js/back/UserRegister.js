// 用户注册名必须是两到三个汉字
function checkUsername() {
    return checkText("username", "usernameTip", /^[\u4e00-\u9fa5]{2,5}$/, "格式输入正确", "格式输入错误")
}

// 电话号码必须是合法的
function checkPhone() {
    return checkText("phone", "phoneTip", /^[1]+[3,8]+\d{9}$/, "合法手机号", "非法手机号")
}

// 封装公共方法
function checkText(id, tipId, reg, success, error) {
    let name = $("#" + id).val();
    if (reg.test(name)) {
        $("#" + tipId).html(success);
        $("#" + tipId).css("color", "green   ");
        return true;
    } else {
        $("#" + tipId).html(error);
        $("#" + tipId).css("color", "red");
        return false;
    }
}

// 提交前判断格式是否正确
function judge() {
    let name = checkUsername();
    let phone = checkPhone();
    if (name && phone) {
        let data = {
            username: $("#username").val(),
            password: $("#password").val(),
            sex: $("#sex").val(),
            birthday: $("#birthday").val(),
            phone: $("#phone").val(),
            introduce: $("#introduce").val()
        };
        $.ajax({
            url: '/UserRegiter',//对应@WebServlet
            data: data,//传递到后台的值
            type: 'post',
            dataType: 'json',//后台传到前端的值的类型 text html application json
            success: function (data) {//后台传递到前端的值
                if (data.count==1){
                    alert("注册成功");
                    window.open("/back/login.html");
                }else if (data.count==-1) {
                 alert("用户名已存在");
                }else {
                    alert("其他错误")
                }
            }
        })
    } else {
        $("#tijiaoTip").html("格式输入错误");
        $("#tijiaoTip").css("color", "red");
        return false;
    }


}