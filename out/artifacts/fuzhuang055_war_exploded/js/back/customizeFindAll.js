$(function () {
    findAll();
})

function findAll() {
    let data = myAjax("/back/customize/findAll", {}, "get");
    let html = '';
    data = data.list;
    if (data != null) {
        for (let i = 0; i < data.length; i++) {
            html += '<div>\n' +
                '        <div style="width: 30px">' + data[i].id + '</div>\n' +
                '        <div style="width: 100px">' + data[i].title + '</div>\n' +
                '        <div style="width: 100px">' + data[i].contents + '</div>\n' +
                '        <div style="width: 100px"><img src="' + data[i].imgHref + '" style="width: 50px;height: 50px;"/></div>\n' +
                '        <div style="width: 100px">' + data[i].creationTime + '</div>\n' +
                '        <div style="width: 100px">' + data[i].updateTime + '</div>\n' +
                '        <div style="width: 100px">' + (data[i].isShow == 1 ? '是' : '否') + '</div>\n' +
                '        <div style="width: 100px">' + (data[i].isRecommend == 1 ? '是' : '否') + '</div>\n' +
                '            <div style="width: 35px">\n' +
                '                <div><a href="javascript:edit(' + data[i].id + ')" >编辑</a></div>\n' +
                '                <div><a href="javascript:del(' + data[i].id + ')" >删除</a></div>\n' +
                '            </div>\n' +
                '    </div>';
        }
    }
    $("#customizeFindAll").html(html);
}
function edit(id) {
    localStorage.setItem("id",id);
    $("#right").load("/back/customize/customizeEdit.html");
}
function del(id) {
    let data = myAjax("/back/customize/del", {id:id}, "post");
    if (data.count==1){
        findAll();
    } else {
        alert("删除失败")
    }
}