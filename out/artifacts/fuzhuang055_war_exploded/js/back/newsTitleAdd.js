function save() {
    let data = {
        title: $.trim($("#title").val()),
        time: $.trim($("#time").val()),
        creationTime: $.trim($("#creationTime").val()),
        updateTime: $.trim($("#updateTime").val()),
        isShow: $("input[name='isShow']:checked").val(),
        isTop: $("input[name='isTop']:checked").val()
    }
    let result = myAjax("/back/newsTitle/add", data, "post")
    if (result.count == 1) {
        alert("添加成功");
        $("#right").load("/back/newsTitle/newsTitleFindAll.html");
    } else {
        alert("添加失败");
    }
}
