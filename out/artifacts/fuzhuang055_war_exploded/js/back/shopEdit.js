$(function () {
    findById();
})

function findById() {
    let id=localStorage.getItem("id")
    let data = myAjax("/back/shop/shopFindById", {id: id}, "post")
    if (data!=null){
        data=data.shopModel;
        $("#name").val(data.name);
        $("#address").val(data.address);
        $("#href").attr("src", data.imgHref);
        $("input[name=isShow][value=" + data.isShow + "]").attr("checked", true);
        $("input[name=isTop][value=" + data.isTop + "]").attr("checked", true);
    }
}
function edit() {
    let data=new FormData();
    data.append("name",$("#name").val())
    data.append("address",$("#address").val());
    data.append("imgHref",$("#imgHref")[0].files[0]);
    data.append("isShow",$("input[name='isShow']:checked").val());
    data.append("isTop",$("input[name='isTop']:checked").val());
    data.append("id",localStorage.getItem("id"))
    $.ajax({
        url: "/back/shop/shopEdit",//链接后台的地址：对应webservlet里面的值
        data: data,//前端传递给后台的值
        type: "post",//对应servlet里面的doget或者dopost
        async: false,
        processData:false,
        contentType:false,
        dataType: 'json',//统一从后台传递给前端格式
        success: function (data) {//后台传递给前端的值
            if (data.count == 1) {
                alert("編輯成功")
                $("#right").load("/back/shop/shopFindAll.html");
            } else {
                alert("編輯失败");
            }
        }
    })
}