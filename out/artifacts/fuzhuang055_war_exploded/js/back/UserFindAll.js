$(function () {
    $("#page").load("/back/page.html");
    findAll();

})

function findAll() {
    let da = {};
    //查找页面ID的值
    da.userName = $.trim($("#name").val());
    da.startBirthday = $.trim($("#startBirthday").val());
    da.endBirthday = $.trim($("#endBirthday").val());
    da.introduce = $.trim($("#introduce").val());
    da.sex = $("#sex").find("option:selected").text();
    da.phone = $.trim($("#phone").val());
    //分页
    da.pageNum = $.trim($("#pageNum").val());
    da.pageSize = $.trim($("#pageSize").val());
    console.log(da)
    let data = myAjax("/back/user/findAll", da, "get");
    let html = '';
    //分页数据总条数
    $("#count").html(data.count);
    data = data.list;
    if (data != null) {
        for (let i = 0; i < data.length; i++) {
            //显示数据库里存在的值
            html += '<div><div style="width: 30px">' + data[i].id + '</div>\n' +
                '        <div style="width: 50px">' + data[i].username + '</div>\n' +
                '        <div style="width: 100px">' + data[i].birthday + '</div>\n' +
                '        <div style="width: 100px">' + data[i].introduce + '</div>\n' +
                '        <div style="width: 36px">' + data[i].sex + '</div>\n' +
                '        <div style="width: 130px">' + data[i].phone + '</div>\n' +
                '        <div style="width: 100px">' + data[i].creationTime + '</div>\n' +
                '        <div style="width: 100px">' + data[i].updateTime + '</div>\n' +
                '        <div style="width: 35px">' + (data[i].isShow == 1 ? '是' : '否') + '</div>\n' +
                '        <div style="width: 35px">' + (data[i].isTop == 1 ? '是' : '否')+ '</div>\n' +
                '            <div style="width: 35px">\n' +
                '                <div><a href="javascript:edit(' + data[i].id + ')" >编辑</a></div>\n' +
                '                <div><a href="javascript:del(' + data[i].id + ')" >删除</a></div>\n' +
                '            </div>\n' +
                '        </div>';
        }
    }
    $("#userFindAll").html(html);
}

function edit(id) {
    // 本地存储Id
    localStorage.setItem("userid", id);
    // session存储
    sessionStorage.setItem("userid", id);
    $('#right').load('/back/user/UserEdit.html')
}

function del(id) {
    let data = myAjax("/back/user/del", {id: id}, "post");
    if (data.count == 1) {
        findAll();
    }
}
