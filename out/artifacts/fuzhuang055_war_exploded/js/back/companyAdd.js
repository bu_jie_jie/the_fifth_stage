function save() {
    let data = {
        address: $.trim($("#address").val()),
        phone: $.trim($("#phone").val()),
        qq: $.trim($("#qq").val()),
        Email: $.trim($("#Email").val()),
        creationTime: $.trim($("#creationTime").val()),
        updateTime: $.trim($("#updateTime").val()),
        isShow: $("input[name='isShow']:checked").val(),
        isTop: $("input[name='isTop']:checked").val()
    }
    let result = myAjax("/back/company/add", data, "post")
    if (result.count == 1) {
        alert("添加成功");
        $("#right").load("/back/company/companyFindAll.html");
    } else {
        alert("添加失败");
    }
}
