$(function () {
    findUsernameFromBack();
})

function findUsernameFromBack() {
    let data = myAjax("/manage", {}, "get");
    if (data.username == null) {
        window.open("/back/login.html");
    } else {
        $("#loginUserName").html(data.username);
    }
}