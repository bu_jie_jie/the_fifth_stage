$(function () {
    getDate();
})
function getDate() {
    //获取本地的Id
    let id=localStorage.getItem("id");
    let data=myAjax("/back/company/companyFindById",{id:id},"post");
    if (data!=null){
        data=data.companyModel;
        $("#address").val(data.address);
        $("#phone").val(data.phone);
        $("#qq").val(data.qq);
        $("#Email").val(data.mail);
        $("input[name=isShow][value=" + data.isShow + "]").attr("checked", true);
        $("input[name=isTop][value=" + data.isTop + "]").attr("checked", true);
    }
}


function edit(){
    let data={
        //获取session的ID
        id:sessionStorage.getItem("id"),
        address:$("#address").val(),
        phone:$("#phone").val(),
        qq:$("#qq").val(),
        mail:$("#Email").val(),
        isShow:$("input[name='isShow']:checked").val(),
        isTop:$("input[name='isTop']:checked").val()
    };
    let da=myAjax("/back/company/companyEdit",data,"post");
    if (da.count==1){
        alert("编辑成功");
        $('#right').load('/back/company/companyFindAll.html');
    } else {
        alert("编辑失败");
    }
}
