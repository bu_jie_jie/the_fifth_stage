$(function () {
    getDate();
})
function getDate() {
    //获取本地的Id
    let id=localStorage.getItem("id");
    let data=myAjax("/back/clothing/clothingFindById",{id:id},"post");
    if (data!=null){
        data=data.clothingModel;
        $("#name").val(data.name);
        $("input[name=isShow][value=" + data.isShow + "]").attr("checked", true);
        $("input[name=isTop][value=" + data.isTop + "]").attr("checked", true);
    }
}


function edit(){
    let data={
        //获取iD
        id:localStorage.getItem("id"),
        name:$("#name").val(),
        isShow:$("input[name='isShow']:checked").val(),
        isTop:$("input[name='isTop']:checked").val()
    };
    let da=myAjax("/back/clothing/clothingEdit",data,"post");
    if (da.count==1){
        alert("编辑成功");
        $('#right').load('/back/clothing/clothingFindAll.html');
    } else {
        alert("编辑失败");
    }
}
