         $(function () {
    getData();
})
function getData() {
    //獲取ID
    let id=localStorage.getItem("userid");
    let data=myAjax("/back/user/findById",{id:id},"get");
    if (data!=null){
        data =data.userModel;
        // $("#userid").val(data.id);
        $("#username").val(data.username);
        $("#password").val(data.password);
        $("#sex").val(data.sex);
        $("#birthday").val(data.birthday);
        $("#phone").val(data.phone);
        // $("#creationTime").val(data.creationTime);
        // $("#updateTime").val(data.updateTime);
        $("input[name=isShow][value=" + data.isShow + "]").attr("checked", true);
        $("input[name=isTop][value=" + data.isTop + "]").attr("checked", true);
        $("#introduce").val(data.introduce);
    }
}
function save() {
    let data = {
        id:localStorage.getItem("userid"),
        username: $("#username").val(),
        password: $("#password").val(),
        sex: $("#sex").val(),
        birthday: $("#birthday").val(),
        phone: $("#phone").val(),
        // creationTime:$("#creationTime").val(),
        // updateTime:$("#updateTime").val(),
        isShow:$("input[name='isShow']:checked").val(),
        isTop:$("input[name='isTop']:checked").val(),
        introduce: $("#introduce").val()
    };
    $.ajax({
        url: '/back/user/edit',//对应@WebServlet
        data: data,//传递到后台的值
        type: 'post',
        dataType: 'json',//后台传到前端的值的类型 text html application json
        success: function (data) {//后台传递到前端的值
           if (data.count==1){
               $("#right").load("/back/user/UserFindAll.html");
           } else {
               alert("编辑失败");
           }
        }
    })
}
