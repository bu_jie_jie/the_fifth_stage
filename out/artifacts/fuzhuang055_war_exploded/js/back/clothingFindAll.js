$(function () {
    findAll();
})

function findAll() {
    let data = myAjax("/back/clothing/findAll", {}, "get");
    let html = '';
    data = data.list;
    if (data != null) {
        for (let i = 0; i < data.length; i++) {
            html += ' <div><div style="width: 50px">' + data[i].id + '</div>\n' +
                '        <div style="width: 150px">' + data[i].name + '</div>\n' +
                '        <div style="width: 100px">' + data[i].createTime + '</div>\n' +
                '        <div style="width: 100px">' + data[i].updateTime + '</div>\n' +
                '        <div style="width: 100px">' + (data[i].isShow == 1 ? '是' : '否') + '</div>\n' +
                '        <div style="width: 100px">' + (data[i].isTop == 1 ? '是' : '否') + '</div>\n' +
                '            <div style="width: 50px">\n' +
                '                <div><a href="javascript:edit(' + data[i].id + ')" >编辑</a></div>\n' +
                '                <div><a href="javascript:del(' + data[i].id + ')" >删除</a></div>\n' +
                '            </div>\n' +
                '        </div>';
        }
    }
    $("#findAll").html(html);
}

function del(id) {
    let data = myAjax("/back/clothing/del", {id: id}, "post");
    if (data.count == 1) {
        findAll();
    } else {
        alert("删除失败")
    }
}
function edit(id) {
    localStorage.setItem("id",id);
    $("#right").load("/back/clothing/clothingEdit.html");
}