function myAjax(url, data, type) {
    let result = null;
    $.ajax({
        url: url,//链接后台的地址：对应webservlet里面的值
        data: data,//前端传递给后台的值
        type: type,//对应servlet里面的doget或者dopost
        async: false,

        dataType: 'json',//统一从后台传递给前端格式
        success: function (data) {//后台传递给前端的值
            console.log(data);
            result = data;
        }
    })
    return result;
}