<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 15:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<footer>
    <div class="footerto">
        <div class="footerleft">
            <c:forEach items="${newsTitleModels}" var="newsTitleModels" varStatus="shu">
                <c:if test="${shu.index<3}">
                    <div class="footline">
                        <div class="footword">${newsTitleModels.title}</div>
                        <div class="foottime">${newsTitleModels.time}</div>
                    </div>
                </c:if>
            </c:forEach>
        </div>
        <div class="footermiddle">
            <div class="middle-top">${companyModel.name}</div>
            <div class="middle-down">地址：${companyModel.address}</div>
            <div class="middle-down">电话：${companyModel.phone}</div>
            <div class="middle-down">邮件：${companyModel.mail}</div>
        </div>
        <div class="footerright">${companyModel.contents}</div>
    </div>
</footer>