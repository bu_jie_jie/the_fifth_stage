<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>关于我们</title>
    <link rel="stylesheet" href="/css/css.css" type="text/css">
    <link rel="stylesheet" href="/css/apprectation.css">
    <link rel="stylesheet" href="/css/glamoring.css">
<%--    <script src="js/jquery.js"></script>--%>
<%--    <script src="js/common.js"></script>--%>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>

<section>
    <!-- 标题盒子-->
    <div class="u">
        <div class="section-top">
            <div class="section-box">
                <P class="p1">关于我们</P>
                <p class="p2">ABOUT US</p>
            </div>
            <div class="section-in">
            </div>
        </div>
    </div>
    <!-- end标题 -->
    <div class="bgm">
        <div class="bgm-on">
            <div class="bgm-left">
                <img src="/img/4.jpg">
            </div>
            <div class="bgm-right">
                <div class="r-top">GLAMORING</div>
                <div class="r-u">GLAMORING致力于创新的风格，卓越的品质并且追求与众不同，专注于顶尖
                    的设计和非凡的工艺，在过去的11年里毫不意外地占据了巨大的市场份额，成
                    为世界各地最受欢迎的品牌之一。该品牌继续保持卓越的品质，同时不断推出
                    杰出的设计，为全世界的新娘们铺平道路。高级婚纱定制将每对爱情化为设计
                    元素，将婚纱脱离了仅限于扮演婚礼中一美丽道具的角色进而成为了见证爱情
                    的传承之品。那是独一无二，无可取代的另一种爱情结晶的表现。因为，那是
                    专属于每个新娘的幸福象征，也可延续此幸福至后代。
                </div>
                <div class="r-bottom">
                    <div class="r-b-in">
                        <div class="b-in"><img src="/img/i1.png">
                            <P>砖石品质</P></div>
                        <div class="b-in"><img src="/img/i2.png">
                            <p>高端定制</p></div>
                        <div class="b-in"><img src="/img/i3.png">
                            <p>极致服务</p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    设计师介绍-->

    <div class="setshow">
        <!-- 标题盒子-->
        <div class="u">
            <div class="section-top">
                <div class="section-box">
                    <P class="p1">设计师介绍</P>
                    <p class="p2">DESIGNER INTRODUCTION</p>
                </div>
                <div class="section-in">
                </div>
            </div>
        </div>
        <!-- end标题 -->
        <div class="shejishi-tu">
            <div class="shejishi-show-pictuer ">
                <img src="/img/c1.jpg">
            </div>
            <div class="shejishi-show-pictuer shejishi-show-pictuer-left">
                <img src="/img/c2.jpg">
            </div>
            <div class="shejishi-show-pictuer shejishi-show-pictuer-left">
                <img src="/img/c3.jpg">
            </div>
            <div class="shejishi-show-pictuer shejishi-show-pictuer-right">
                <img src="/img/c4.jpg">
            </div>
        </div>
        <div class="jieshao">
            <div class="xiangxijieshao">
                <div class="jieshao1">设计师：miko</div>
                <div class="jieshao2">从事婚纱行业13年，主设计婚纱礼服200多套，专攻婚纱设计，礼服设计。</div>
            </div>
            <div class="xiangxijieshao">
                <div class="jieshao1">设计师：lin</div>
                <div class="jieshao2">美国XXX设计学院毕业，曾获得XXXX设计奖。主导设计婚纱90套</div>
            </div>
            <div class="xiangxijieshao">
                <div class="jieshao1">设计师：sandy</div>
                <div class="jieshao2">从事婚纱行业7年，主设计婚纱礼服68多套，专攻礼服设计。</div>

            </div>
            <div class="jieshaoright">
                <div class="jieshao1">设计师：kay</div>
                <dv class="jieshao2">曾服务过150对新人，设计的婚纱和西装都获得一致好评</dv>
            </div>
        </div>

    </div>
    <div class="down"></div>
    <!-- 标题盒子-->
    <div class="u">
        <div class="section-top">
            <div class="section-box">
                <P class="p1">线下门店</P>
                <p class="p2">PFFLINE  STORE</p>
            </div>
            <div class="section-in">
            </div>
        </div>
    </div>
    <!-- end标题 -->
    <!--    线下门店-->
    <div class="lunbo">
        <div class="xianxiaright">
            <div class="datu">
                <img src="/img/7504.jpg">
            </div>
            <div class="xiaotu">
                <img src="/img/7502.jpg">
            </div>
            <div class="xiaotu">
                <img src="/img/7503.jpg">
            </div>
            <div class="xiaotu">
                <img src="/img/7504.jpg">
            </div>
            <div class="xiaotu">
                <img src="/img/750.jpg">
            </div>
        </div>
        <div class="xianxialeft">
            <div>线下门店地址</div>
            <div>上海市xxxxx区xxxxx路xxxx号（xxx店）</div>
            <div>上海市xxxxx区xxxxx路xxxx号（xxx店）</div>
            <div>上海市xxxxx区xxxxx路xxxx号（xxx店）</div>
            <div>上海市xxxxx区xxxxx路xxxx号（xxx店）</div>
        </div>
    </div>
    <!-- end   线下门店-->
    <!--    设计师介绍-->
</section>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
