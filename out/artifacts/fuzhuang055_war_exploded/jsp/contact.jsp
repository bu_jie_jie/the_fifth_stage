<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>联系我们</title>
    <link rel="stylesheet" href="/css/css.css">
    <link rel="stylesheet" href="/css/contact.css">
<%--    <script src="js/jquery.js"></script>--%>
<%--    <script src="js/common.js"></script>--%>
    <script type="text/javascript" src="//api.map.baidu.com/api?type=webgl&v=1.0&ak=ZWZTzzN3OyFYtcIZEuM8vf2AEniv4H32"></script>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>

<section>
    <!-- 标题盒子-->
    <div class="u">
        <div class="section-top">
            <div class="section-box">
                <P class="p1">高定制服务</P>
                <p class="p2">HIGH-END CUSTOMIZATION</p>
            </div>
            <div class="section-in">
            </div>
        </div>
    </div>
    <!-- end标题 -->
    <div class="bgm1">
        <div class="bgm-on1">
            <div class="bgm-left1">
                <img src="/img/4.jpg">
            </div>
            <div class="bgm-right1">
                <div class="r-top1">
                    <div class="r-top1-title">
                        <h2>联系我们</h2>
                    </div>
                    <div>
                        <h4>地址：四川省成都市孵化园</h4>
                        <h4>电话：028-000000</h4>
                        <h4>Q Q：123456789</h4>
                        <h4>邮箱：admin@163.com</h4>

                    </div>
                </div>
                <div class="r-u1">
                    <div class="r-top1-title">
                        <h2>在线咨询</h2>
                    </div>
                    <div class="hanghezi">
                        <div class="lefthezi">姓名</div>
                        <div class="righthzi"><input type="text" placeholder="请输入姓名" maxlength="5"></div>
                    </div>

                    <div class="hanghezi">
                        <div class="lefthezi">手机号码</div>
                        <div class="righthzi"><input type="text" placeholder="请输入手机号码" maxlength="11"></div>
                    </div>

                    <div class="hanghezi2">
                        <div class="lefthezi">咨询内容</div>
                        <div class="righthzi"><textarea class="input2"  placeholder="请输入咨询内容"></textarea></div>
                    </div>
                    <div class="hanghezi">
                        <div class="lefthezi"></div>
                        <div class="righthzi"><input class="input3" type="submit" value="提交"></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <!--            <div class="map"><iframe src="map.html" frameborder="0" scrolling="no"></iframe></div>-->
            <div class="map" id="allmap">  <script src="/js/map.js"></script></div>
        </div>
    </div>
</section>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
