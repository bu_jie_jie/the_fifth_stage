<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>作品展示</title>
    <link rel="stylesheet" href="/css/css.css" type="text/css">
    <link rel="stylesheet" href="/css/apprectation.css">
<%--    <script src="js/jquery.js"></script>--%>
<%--    <script src="js/common.js"></script>--%>

</head>
<body>
<jsp:include page="header.jsp"></jsp:include>

<section>
    <!-- 标题盒子-->
    <div class="u">
        <div class="section-top">
            <div class="section-box">
                <P class="p1">作品欣赏</P>
                <p class="p2">APPRECIATION OF WORKS</p>
            </div>
            <div class="section-in">
            </div>
        </div>
    </div>
    <!-- end标题 -->
    <!--    展示图-->
    <div class="show">
        <div class="show-top">
            <div class="show-title ">婚纱</div>
            <div class="show-title ">礼服</div>
            <div class="show-title ">西装</div>
            <div class="show-title ">定制</div>
        </div>
        <!--        婚纱加价格-->
        <div class="tu">
            <div class="show-pictuer ">
                <img src="/img/pro14.jpg">
            </div>
            <div class="show-pictuer show-pictuer-left">
                <img src="/img/pro15.jpg">
            </div>
            <div class="show-pictuer show-pictuer-left">
                <img src="/img/pro16.jpg">
            </div>
            <div class="show-pictuer show-pictuer-right">
                <img src="/img/pro17.jpg">
            </div>
        </div>
        <div class="name-all">
            <div class="kuang">
                <div class="name">純黑西裝</div>
                <div class="much">
                    价格：<span>777.00</span>
                </div>
            </div>
            <div class="kuang">
                <div class="name">灰色西裝</div>
                <div class="much">
                    价格：<span>55.00</span>
                </div>
            </div>
            <div class="kuang">
                <div class="name">白色西裝</div>
                <div class="much">
                    价格：<span>55.00</span>
                </div>
            </div>
            <div class="kuang">
                <div class="name">面容西裝</div>
                <div class="much">
                    价格：<span>99.00</span>
                </div>
            </div>
        </div>
        <!--     end   婚纱加价格-->

        <!--        婚纱加价格-->
        <div class="tu">
            <div class="show-pictuer ">
                <img src="/img/pro18.jpg">
            </div>
            <div class="show-pictuer show-pictuer-left">
                <img src="/img/pro19.jpg">
            </div>
            <div class="show-pictuer show-pictuer-left">
                <img src="/img/pro20.jpg">
            </div>
        </div>
        <div class="name-all">
            <div class="kuang">
                <div class="name">絲綢面西裝</div>
                <div class="much">
                    价格：<span>99.00</span>
                </div>
            </div>
            <div class="kuang">
                <div class="name">白色光面西裝</div>
                <div class="much">
                    价格：<span>5555.00</span>
                </div>
            </div>
            <div class="kuang">
                <div class="name">深藍色西裝</div>
                <div class="much">
                    价格：<span>66666.00</span>
                </div>
            </div>
        </div>
        <!--     end   婚纱加价格-->
        <jsp:include page="fenye.jsp"></jsp:include>
    </div>
    <!--    展示图-->
</section>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
