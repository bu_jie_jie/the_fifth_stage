<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.cn.wanxi.model.NewsModel" %><%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 15:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
<head>
    <meta charset="UTF-8">

</head>
<body>
<!--图片加文字-->
<header>
    <div class="header-div">
        <img src="${topModel.imgHref}">
        ${topModel.contents}
    </div>
</header>
<!--end 图加文-->
<!--导航栏部分-->
<nav>
    <div class="nav-out">
        <c:forEach items="${navList}" var="navList" varStatus="aa">
            <a href="${navList.src}">
                <c:if test="${aa.index==0}">
                    <div class="nav-left"> ${navList.name}</div>
                </c:if>
                <c:if test="${aa.index>0}">
                    <div class="nav-1">${navList.name}</div>
                </c:if>
            </a>
        </c:forEach>
    </div>
</nav>
<!--end导航栏-->
</body>
</html>