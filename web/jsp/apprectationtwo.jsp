<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>作品展示</title>
    <link rel="stylesheet" href="/css/css.css" type="text/css">
    <link rel="stylesheet" href="/css/apprectation.css">
<%--    <script src="../js/jquery.js"></script>--%>
<%--    <script src="../js/common.js"></script>--%>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>

<section>
    <!-- 标题盒子-->
    <div class="u">
        <div class="section-top">
            <div class="section-box">
                <P class="p1">作品欣赏</P>
                <p class="p2">APPRECIATION OF WORKS</p>
            </div>
            <div class="section-in">
            </div>
        </div>
    </div>
    <!-- end标题 -->
    <!--    展示图-->
    <div class="show">
        <div class="show-top">
            <div class="show-title ">婚纱</div>
            <div class="show-title ">礼服</div>
            <div class="show-title ">西装</div>
            <div class="show-title ">定制</div>
        </div>
        <!--        婚纱加价格-->
        <div class="tu">
            <div class="show-pictuer ">
                <img src="/img/pro7.jpg">
            </div>
            <div class="show-pictuer show-pictuer-left">
                <img src="/img/pro8.jpg">
            </div>
            <div class="show-pictuer show-pictuer-left">
                <img src="/img/pro9.jpg">
            </div>
            <div class="show-pictuer show-pictuer-right">
                <img src="/img/pro10.jpg">
            </div>
        </div>
        <div class="name-all">

            <div class="kuang">
                <div class="name">抹胸婚纱</div>
                <div class="much">
                    价格：<span>99.00</span>
                </div>
            </div>

            <div class="kuang">
                <div class="name">V胸婚纱</div>
                <div class="much">
                    价格：<span>5555.00</span>
                </div>
            </div>

            <div class="kuang">
                <div class="name">小香肩婚纱</div>
                <div class="much">
                    价格：<span>66666.00</span>
                </div>
            </div>

            <div class="kuang">
                <div class="name">披肩婚纱</div>
                <div class="much">
                    价格：<span>99.00</span>
                </div>
            </div>
        </div>
        <!--     end   婚纱加价格-->

        <!--        婚纱加价格-->
        <div class="tu">
            <div class="show-pictuer ">
                <img src="/img/pro11.jpg">
            </div>
            <div class="show-pictuer show-pictuer-left">
                <img src="/img/pro13.jpg">
            </div>
            <div class="show-pictuer show-pictuer-left">
                <img src="/img/pro13.jpg">
            </div>

        </div>
        <div class="name-all">

            <div class="kuang">
                <div class="name">抹胸婚纱</div>
                <div class="much">
                    价格：<span>99.00</span>
                </div>
            </div>

            <div class="kuang">
                <div class="name">V胸婚纱</div>
                <div class="much">
                    价格：<span>5555.00</span>
                </div>
            </div>

            <div class="kuang">
                <div class="name">小香肩婚纱</div>
                <div class="much">
                    价格：<span>66666.00</span>
                </div>
            </div>
        </div>
        <!--     end   婚纱加价格-->
        <jsp:include page="fenye.jsp"></jsp:include>
    </div>
    <!--    展示图-->
</section>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
