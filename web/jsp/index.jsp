<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 15:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>服装定制</title>
    <link rel="stylesheet" href="/css/css.css">
    <%--    <script src="js/jquery.js"></script>--%>
    <%--    <script src="js/common.js"></script>--%>
</head>
<body>

<!--图片加文字-->
<jsp:include page="header.jsp"></jsp:include>
<section>
    <div class="section-one">
        <!-- 标题盒子-->
        <div class="section-top">
            <a href="/jsp/dingzhifuwu.jsp">
                <div class="section-box">
                    <P class="p1">高端定制服务</P>
                    <p class="p2">HIGH-END CUSTOMIZATION</p>
                </div>
            </a>
            <div class="section-in">
            </div>
        </div>

        <!-- end盒子 -->
        <!-- 三图-->
        <div class="pictuer">
            <div class="pictuer-left">
                <img src="/img/1.jpg">
            </div>
            <div class="pictuer-u">
                <img src="/img/2.jpg">
            </div>
            <div class="pictuer-right">
                <img src="/img/3.jpg">
            </div>
        </div>
    </div>
    <!-- end 图-->
    <div class="bgm">
        <div class="bgm-on">
            <div class="bgm-left">
                <img src="${aboutusModel.imgHref}">
            </div>
            <div class="bgm-right">
                <div class="r-top">${companyModel.name}</div>
                <div class="r-u">${aboutusModel.contents}
                </div>
                <div class="r-bottom">
                    <div class="r-b-in">
                        <div class="b-in"><img src="/img/i1.png">
                            <P>砖石品质</P></div>
                        <div class="b-in"><img src="/img/i2.png">
                            <p>高端定制</p></div>
                        <div class="b-in"><img src="/img/i3.png">
                            <p>极致服务</p></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="apprectatio">
        <!-- 标题盒子-->
        <div class="u">
            <div class="section-top">
                <a href="apprectation">
                    <div class="section-box">
                        <P class="p1">作品欣赏</P>
                        <p class="p2">APPRECIATION OF WORKS</p>
                    </div>
                </a>
                <div class="section-in">
                </div>
            </div>
        </div>
        <!-- end标题 -->
        <!--    卡片-->

        <c:forEach items="${workList}" var="workList" varStatus="ff">
            <div class="card">
                <c:if test="${ff.index%2==0}">
                    <div class="card-left"><img src="${workList.imgHref}"></div>
                    <div class="card-img"><img src="${workList.pictureHref}"></div>
                    <div class="card-right">
                        <div class="c-r-in">
                            <p>
                                    ${workList.contentMan}
                            </p>
                            <p>
                                    ${workList.contentWoman}
                            </p>
                        </div>
                    </div>
                </c:if>
                <c:if test="${ff.index%2!=0}">
                    <div class="c-two">
                        <div class="c-l-in">
                            <p>
                                    ${workList.contentMan}
                            </p>
                            <p>
                                    ${workList.contentWoman}
                            </p>
                        </div>
                    </div>
                    <div class="card-img"><img src="${workList.pictureHref}"></div>
                    <div class="card-left"><img src="${workList.imgHref}"></div>
                </c:if>
            </div>
        </c:forEach>
    </div>
    <!--    新闻动态-->
    <div class="news">
        <div class="new">
            <!-- 标题盒子-->
            <div class="u">
                <div class="section-top">
                    <a href="news">
                        <div class="section-box">
                            <P class="p1">新闻动态</P>
                            <p class="p2">NEWS INFORMATION</p>
                        </div>
                    </a>
                    <div class="section-in">
                    </div>
                </div>
            </div>
            <!-- end标题 -->
            <div class="newslist">
                <c:forEach items="${newsTitleModels}" var="newsTitleModels" varStatus="news">
                    <c:if test="${news.index<6}">
                        <div class="listsy">
                            <div class="listadd">${newsTitleModels.adds}</div>
                            <div class="word">${newsTitleModels.title}</div>
                            <div class="time">${newsTitleModels.time}</div>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
            <div class="newspictuer">
                <img src="/img/8.jpg">
            </div>
        </div>
    </div>
    <!--    end新闻动态-->
</section>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>