<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>新闻动态</title>
    <link rel="stylesheet" href="/css/css.css">
    <link rel="stylesheet" href="/css/news.css">
<%--    <script src="js/jquery.js"></script>--%>
<%--    <script src="js/common.js"></script>--%>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<section>
    <!-- 标题盒子-->
    <div class="u">
        <div class="section-top">
            <div class="section-box">
                <P class="p1">新闻动态</P>
                <p class="p2">NEWS INFORMATON</p>
            </div>
            <div class="section-in">
            </div>
        </div>
    </div>
    <!-- end标题 -->
    <!--    新闻页-->
    <div class="newska">
        <div class="new-left">
            <h3>行业新闻</h3>
            <h3>公司新闻</h3>
            <h3>服装资讯</h3>
        </div>
        <div class="new-right">
            <div class="right-top">
                <img src="/img/w72s.jpg">
                <h3 class="h3" >中国与印度纺织业工业大PK</h3>
                <h4 class="h4">现在大家已经进入了一个史无前例的降低成本的历史时代，
                    纺织厂采用创新技术，提高电力效益， 节约燃料，
                    压缩劳动力，把报酬与部门生产率净增长相联系。【详细】</h4>
                <h4 class="timwh4">2015-03-17</h4>
            </div>
            <div class="right-bottom">
                <c:forEach items="${newsList}" var="newsList">
                <div class="chang">
                    <h4 class="h41">${newsList.content}</h4>
                    <h3 class="h42">${newsList.time}</h3>
                </div>
                </c:forEach>
                <jsp:include page="fenye.jsp"></jsp:include>
            </div>
        </div>
    </div>
    <!--    新闻页-->
</section>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>