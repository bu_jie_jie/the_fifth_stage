<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>高端定制服务</title>
    <link rel="stylesheet" href="/css/css.css">
    <link rel="stylesheet" href="/css/dingzhifuwu.css">
<%--    <script src="js/jquery.js"></script>--%>
<%--    <script src="js/common.js"></script>--%>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<scetion>
    <!-- 标题盒子-->
    <div class="u">
        <div class="section-top">
            <div class="section-box">
                <P class="p1">高定制服务</P>
                <p class="p2">HIGH-END CUSTOMIZATION</p>
            </div>
            <div class="section-in">
            </div>
        </div>
    </div>
    <!-- end标题 -->
    <div class="sectionbg">
        <div class="sectionbgin">
            <div class="title">定制流程</div>
            <!--第一行-->
            <div class="yueshu">
                <c:forEach items="${dingZhiList}" var="dingZhiList" varStatus="aa">
                    <c:if test="${aa.index<3}">
                        <div class="cardone">
                            <div class="blak">
                                <img src="${dingZhiList.img}">
                                <span class="blakspan">${dingZhiList.title}</span>
                            </div>
                            <div class="red">
                                <span>${dingZhiList.content}</span>
                            </div>
                        </div>
                        <c:if test="${aa.index<2}">
                            <div class="cardtwo"><img src="${dingZhiList.jiantou}"></div>
                        </c:if>
                    </c:if>
                </c:forEach>
            </div>
            <!--end第一行-->
            <!--第二行-->
            <c:forEach items="${dingZhiList}" var="dingZhiList" varStatus="aaa">
                <c:if test="${aaa.index==2}">
                    <div class="hang">
                        <img src="${dingZhiList.jiantou}">
                    </div>
                </c:if>
            </c:forEach>
            <!-- end第二行-->
            <!--第三行-->
            <div class="yueshu">
                <c:forEach items="${dingZhiList}" var="dingZhiList" varStatus="aa">
                    <c:if test="${aa.index>2&&aa.index<6}">
                        <div class="cardone">
                            <div class="blak">
                                <img src="${dingZhiList.img}">
                                <span class="blakspan">${dingZhiList.title}</span>
                            </div>
                            <div class="red">
                                <span>${dingZhiList.content}</span>
                            </div>
                        </div>
                        <c:if test="${aa.index>2&&aa.index<5}">
                            <div class="cardtwo"><img class="cardthree" src="${dingZhiList.jiantou}"></div>
                        </c:if>
                    </c:if>
                </c:forEach>
            </div>
            <!--end第三行-->
            <!--第四行-->
            <c:forEach items="${dingZhiList}" var="dingZhiList" varStatus="aaa">
                <c:if test="${aaa.index==5}">
                    <div class="hang4">
                        <img src="${dingZhiList.jiantou}">
                    </div>
                </c:if>
            </c:forEach>
            <!-- end第四行-->

            <!--第五行-->
            <div class="yueshu">
                <c:forEach items="${dingZhiList}" var="dingZhiList" varStatus="aa">
                    <c:if test="${aa.index==6}">
                        <div class="cardone">
                            <div class="blak">
                                <img src="${dingZhiList.img}">
                                <span class="blakspan">${dingZhiList.title}</span>
                            </div>
                            <div class="red">
                                <span>${dingZhiList.content}</span>
                            </div>
                        </div>
                        <div class="fiveR">
                            <div>
                                <h2>注意事项</h2>
                            </div>
                            <h3>1.预约时请提供有效的联系方式和联系人姓名</h3>
                            <h3>2.缴纳定金后还没制作坯衣时，若客户取消订单，
                                可以退回定金；一旦确认坯衣，订单不能取消，定金不能退回。</h3>
                            <h3>3.因本产品是定制产品，一旦确认坯衣，不是质量问题，本店一律不退换</h3>
                        </div>

                    </c:if>
                </c:forEach>
            </div>

            <!--end第五行-->
        </div>
    </div>
</scetion>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>