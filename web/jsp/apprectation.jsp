<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aiss
  Date: 2020/7/9
  Time: 16:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>作品展示</title>
    <link rel="stylesheet" href="/css/css.css" type="text/css">
    <link rel="stylesheet" href="/css/apprectation.css">
    <%--    <script src="js/jquery.js"></script>--%>
    <%--    <script src="js/common.js"></script>--%>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<section>
    <!-- 标题盒子-->
    <div class="u">
        <div class="section-top">
            <div class="section-box">
                <P class="p1">作品欣赏</P>
                <p class="p2">APPRECIATION OF WORKS</p>
            </div>
            <div class="section-in">
            </div>
        </div>
    </div>
    <!-- end标题 -->
    <!--    展示图-->
    <div class="show">
        <div class="show-top">
            <div class="show-title ">婚纱</div>
            <div class="show-title ">礼服</div>
            <div class="show-title ">西装</div>
            <div class="show-title ">定制</div>
        </div>
        <!--        婚纱加价格-->
        <div class="tu">
            <c:forEach items="${workList}" var="workList" varStatus="aa">
                <c:if test="${aa.count<5}">
                    <c:if test="${aa.count==1}">
                        <div class="show-pictuer ">
                            <img src="${workList.img}">
                        </div>
                    </c:if>
                    <c:if test="${aa.count>1&&aa.count<4}">
                        <div class="show-pictuer show-pictuer-left">
                            <img src="${workList.img}">
                        </div>
                    </c:if>
                    <c:if test="${aa.count==4}">
                        <div class="show-pictuer show-pictuer-right">
                            <img src="${workList.img}">
                        </div>
                    </c:if>
                </c:if>
            </c:forEach>
        </div>
        <div class="name-all">
            <c:forEach items="${workList}" var="workList" varStatus="aaa">
                <c:if test="${aaa.index<4}">
                    <div class="kuang">
                        <div class="name">${workList.name}</div>
                        <div class="much">
                            价格：<span>${workList.much}</span>
                        </div>
                    </div>
                </c:if>
            </c:forEach>
        </div>
        <div class="tu">
            <c:forEach items="${workList}" var="workList" varStatus="aa">
                <c:if test="${aa.count>4}">
                    <c:if test="${aa.count==5}">
                        <div class="show-pictuer ">
                            <img src="${workList.img}">
                        </div>
                    </c:if>
                    <c:if test="${aa.count>5&&aa.count<8}">
                        <div class="show-pictuer show-pictuer-left">
                            <img src="${workList.img}">
                        </div>
                    </c:if>
                    <c:if test="${aa.count==8}">
                        <div class="show-pictuer show-pictuer-right">
                            <img src="${workList.img}">
                        </div>
                    </c:if>
                </c:if>
            </c:forEach>
        </div>
        <div class="name-all">
            <c:forEach items="${workList}" var="workList" varStatus="aaa">
                <c:if test="${aaa.index>3}">
                    <div class="kuang">
                        <div class="name">${workList.name}</div>
                        <div class="much">
                            价格：<span>${workList.much}</span>
                        </div>
                    </div>
                </c:if>
            </c:forEach>
        </div>



        <jsp:include page="fenye.jsp"></jsp:include>
    </div>
    <!--    展示图-->
</section>
<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
