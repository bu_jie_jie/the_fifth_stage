function page(type) {
    let pageNum = $("#pageNum").val();
    pageNum = pageNum == "" ? 1 : parseInt(pageNum);
    let pageSize = $("#pageSize").val();
    pageSize = pageSize == "" ? 7 : parseInt(pageSize);
    let count = $("#count").html();
    count = count == "" ? 0 : parseInt(count);
    let countPage =Math.ceil( count / pageSize);
    //判断各个按钮如何赋值
    switch (type) {
        case "GO":
            break;
        case "first":
            pageNum = 1;
            break;
        case "last":
            if (pageNum <= 1) {
                pageNum = 1;
            } else {
                --pageNum;
            }
            break;
        case "next":
            if (pageNum >= countPage) {
                pageNum = countPage;
            } else {
                ++pageNum;
            }
            break;
        case "end":
            pageNum = countPage;

            break;
    }
    //赋值
    $("#pageNum").val(pageNum);
    $("#pageSize").val(pageSize);
    findAll();
}