$(function () {
    findAll();
})

function findAll() {
    let data=myAjax("/back/nav/navFindAll",{},"get");
    let html = '';
    data = data.list;
    if (data != null) {
        for (let i = 0; i < data.length; i++) {
            html += '<div><div style="width: 30px">' + data[i].id + '</div>\n' +
                '        <div style="width: 100px">' + data[i].name + '</div>\n' +
                '        <div style="width: 100px">' + data[i].src + '</div>\n' +
                '        <div style="width: 100px">' + data[i].creationTime + '</div>\n' +
                '        <div style="width: 100px">' + data[i].updateTime + '</div>\n' +
                '        <div style="width: 100px">' + (data[i].isShow == 1 ? '是' : '否') + '</div>\n' +
                '        <div style="width: 100px">' + (data[i].isTop == 1 ? '是' : '否')+ '</div>\n' +
                '            <div style="width: 35px">\n' +
                '                <div><a href="javascript:edit(' + data[i].id + ')" >编辑</a></div>\n' +
                '                <div><a href="javascript:del(' + data[i].id + ')" >删除</a></div>\n' +
                '            </div>\n' +
                '        </div>';

        }
    }
    //显示到页面
    $("#navFindAll").html(html);
}

function del(id) {
    let data = myAjax("/back/nav/del", {id: id}, "post");
    if (data.count == 1) {
        alert("删除成功");
        findAll();
    }else {
        alert("删除失败")
    }
}
function edit(id) {
    // 本地存储Id
    localStorage.setItem("id", id);
    // session存储
    sessionStorage.setItem("id", id);
    $('#right').load('/back/nav/navEdit.html');
}