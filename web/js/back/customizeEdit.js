$(function () {
    findById();
})

function findById() {
    let id=localStorage.getItem("id")
    let data = myAjax("/back/customize/findById", {id: id}, "post")
    if (data!=null){
        data=data.customizeModel;
        $("#title").val(data.title);
        $("#contents").val(data.contents);
        $("#href").attr("src", data.imgHref);
        $("input[name=isShow][value=" + data.isShow + "]").attr("checked", true);
        $("input[name=isTop][value=" + data.isTop + "]").attr("checked", true);
    }
}
function save() {
    let data=new FormData();
    data.append("title",$("#title").val())
    data.append("contents",$("#contents").val());
    data.append("imgHref",$("#imgHref")[0].files[0]);
    data.append("isShow",$("input[name='isShow']:checked").val());
    data.append("isTop",$("input[name='isTop']:checked").val());
    data.append("id",localStorage.getItem("id"))
    $.ajax({
        url: "/back/customize/edit",//链接后台的地址：对应webservlet里面的值
        data: data,//前端传递给后台的值
        type: "post",//对应servlet里面的doget或者dopost
        async: false,
        processData:false,
        contentType:false,
        dataType: 'json',//统一从后台传递给前端格式
        success: function (data) {//后台传递给前端的值
            if (data.count == 1) {
                alert("編輯成功")
                $("#right").load("/back/customize/customizeFindAll.html");
            } else {
                alert("編輯失败");
            }
        }
    })
}