function save() {
    if (checkName()) {

        let data = {
            name: $.trim($("#name").val()),
            isShow: $("input[name='isShow']:checked").val(),
            isTop: $("input[name='isTop']:checked").val(),
        }
        let result = myAjax("/back/clothing/add", data, "post")
        if (result.count == 1) {
            $("#right").load("/back/clothing/clothingFindAll.html");
        } else {
            alert("添加失败");
        }
    }
}

function checkName() {
    // trim去首位空格
    let name = $.trim($("#name").val());
    let reg = /[\u4e00-\u9fa5]{2,9}/;
    if (reg.test(name)) {
        return true;
    }
    alert("名字不对");
    return false;
}