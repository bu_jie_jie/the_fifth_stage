function save() {
    let data=new FormData();
    data.append("title",$("#title").val())
    data.append("contents",$("#contents").val());
    data.append("imgHref",$("#imgHref")[0].files[0]);
    data.append("isShow",$("input[name='isShow']:checked").val());
    data.append("isTop",$("input[name='isTop']:checked").val());

    $.ajax({
        url: "/back/aboutus/add",//链接后台的地址：对应webservlet里面的值
        data: data,//前端传递给后台的值
        type: "post",//对应servlet里面的doget或者dopost
        async: false,
        processData:false,
        contentType:false,
        dataType: 'json',//统一从后台传递给前端格式
        success: function (data) {//后台传递给前端的值
            if (data.count == 1) {
                alert("添加成功")
                $("#right").load("/back/aboutus/aboutusFindAll.html");
            } else {
                alert("添加失败");
            }
        }
    })
}