package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.TopDao;
import com.cn.wanxi.model.TopModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TopImpl implements TopDao {
    @Override
    public TopModel getTopModelModelForFront() {
        String sql="select * FROM top";
        ResultSet resultSet= JDBC.getResultSet(sql);
        TopModel topModel=null;
        try {
            while (resultSet.next()){
                topModel=new TopModel();
                topModel.setId(resultSet.getInt("id"));
                topModel.setImgHref(resultSet.getString("img_href"));
                topModel.setContents(resultSet.getString("contents"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return topModel;
    }

}
