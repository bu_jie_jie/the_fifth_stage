package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.IUserDao;
import com.cn.wanxi.model.UserModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserImpl implements IUserDao {
    @Override
    public int add(UserModel userModel) {
        String sql = "insert into user(name,password,sex,birthday,phone,introduce) " +
                "values('" + userModel.getUsername() + "'," +
                "'" + userModel.getPassword() + "'," +
                "'" + userModel.getSex() + "'," +
                "'" + userModel.getBirthday() + "'," +
                "'" + userModel.getPhone() + "'," +
                "'" + userModel.getIntroduce() + "')";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public boolean findByUsername(String username) {
        String sql = "select * from user where name='" + username + "'";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            while (resultSet.next()) {
                isHave = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isHave;
    }

    /**
     * 根据用户名和密码查询，如果存在则返回true，不存在返回false
     *
     * @param userModel
     * @return
     */
    @Override
    public boolean findByUsernameAndPassword(UserModel userModel) {
        String sql = "select *from user where name='" + userModel.getUsername() + "' and password='" + userModel.getPassword() + "'";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            while (resultSet.next()) {
                isHave = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }

    @Override
    public List<UserModel> findAll(UserModel userModel) {
        StringBuilder sql = new StringBuilder("SELECT * FROM user WHERE 1=1 ");
        if (!"".equals(userModel.getUsername()) && userModel.getUsername() != null) {
            sql.append("AND name LIKE '%").append(userModel.getUsername()).append("%' \n");
        }
        if (!"全部".equals(userModel.getSex())) {
            sql.append("AND sex='").append(userModel.getSex()).append("'\n");
        }
        if (!"".equals(userModel.getStartBirthday()) && !"".equals(userModel.getEndBirthday())) {
            sql.append("AND DATE_FORMAT( birthday,'%Y-%m-%d') BETWEEN '").append(userModel.getStartBirthday()).append("' AND '").append(userModel.getEndBirthday()).append("'\n");
        } else {
            if (!"".equals(userModel.getStartBirthday())) {
                sql.append("AND DATE_FORMAT( birthday,'%Y-%m-%d') >='").append(userModel.getStartBirthday()).append("'");
            }
            if (!"".equals(userModel.getEndBirthday())) {
                sql.append("AND DATE_FORMAT( birthday,'%Y-%m-%d') <='").append(userModel.getEndBirthday()).append("'");
            }
        }
        if (!"".equals(userModel.getPhone())) {
            sql.append("AND phone LIKE '%").append(userModel.getPhone()).append("%'\n");
        }
        if (!"".equals(userModel.getIntroduce())) {
            sql.append("AND introduce LIKE '%").append(userModel.getIntroduce()).append("%'");
        }
        sql.append("limit ").append((userModel.getPageModel().getPageNum() + -1) * userModel.getPageModel().getPageSize()).append(",").append(userModel.getPageModel().getPageSize());
        ResultSet resultSet = JDBC.getResultSet(sql.toString());
        List<UserModel> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                UserModel model = new UserModel();
                model.setId(resultSet.getInt("id"));
                model.setUsername(resultSet.getString("name"));
                model.setBirthday(resultSet.getString("birthday"));
                model.setPhone(resultSet.getString("phone"));
                model.setSex(resultSet.getString("sex"));
                model.setIntroduce(resultSet.getString("introduce"));
                model.setCreationTime(resultSet.getString("creation_time"));
                model.setUpdateTime(resultSet.getString("update_time"));
                model.setIsShow(resultSet.getInt("isshow"));
                model.setIsTop(resultSet.getInt("istop"));
                list.add(model);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from user where id='" + id + "'";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public UserModel findById(int id) {
        String sql = "select * from user where id='" + id + "'";
        ResultSet resultSet = JDBC.getResultSet(sql);
        UserModel userModel = new UserModel();
        try {
            while (resultSet.next()) {
                userModel.setId(resultSet.getInt("id"));
                userModel.setPassword(resultSet.getString("password"));
                userModel.setUsername(resultSet.getString("name"));
                userModel.setBirthday(resultSet.getString("birthday"));
                userModel.setPhone(resultSet.getString("phone"));
                userModel.setSex(resultSet.getString("sex"));
                userModel.setIntroduce(resultSet.getString("introduce"));
                userModel.setCreationTime(resultSet.getString("creation_time"));
                userModel.setUpdateTime(resultSet.getString("update_time"));
                userModel.setIsShow(resultSet.getInt("isshow"));
                userModel.setIsTop(resultSet.getInt("istop"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userModel;
    }

    @Override
    public int edit(UserModel userModel) {
        String sql = "update user set name='" + userModel.getUsername() + "'," +
                "password ='" + userModel.getPassword() + "'," +
                "sex='" + userModel.getSex() + "'," +
                "Birthday='" + userModel.getBirthday() + "'," +
                "phone='" + userModel.getPhone() + "'," +
                "introduce='" + userModel.getIntroduce() + "',"  +
//                "creation_time='" + userModel.getCreationTime() + "',"  +
                "update_time='" + userModel.getUpdateTime() + "'," +
                "isshow='" + userModel.getIsShow() + "',"  +
                "istop='" + userModel.getIsTop() + "' " +
                "where id='" + userModel.getId() + "';";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public int getCount(UserModel userModel) {
        StringBuilder sql = new StringBuilder("SELECT count(*) as count FROM user WHERE 1=1 ");
        if (!"".equals(userModel.getUsername()) && userModel.getUsername() != null) {
            sql.append("AND name LIKE '%").append(userModel.getUsername()).append("%' \n");
        }
        if (!"全部".equals(userModel.getSex())) {
            sql.append("AND sex='").append(userModel.getSex()).append("'\n");
        }
        if (!"".equals(userModel.getStartBirthday()) && !"".equals(userModel.getEndBirthday())) {
            sql.append("AND DATE_FORMAT( birthday,'%Y-%m-%d') BETWEEN '").append(userModel.getStartBirthday()).append("' AND '").append(userModel.getEndBirthday()).append("'\n");
        } else {
            if (!"".equals(userModel.getStartBirthday())) {
                sql.append("AND DATE_FORMAT( birthday,'%Y-%m-%d') >='").append(userModel.getStartBirthday()).append("'");
            }
            if (!"".equals(userModel.getEndBirthday())) {
                sql.append("AND DATE_FORMAT( birthday,'%Y-%m-%d') <='").append(userModel.getEndBirthday()).append("'");
            }
        }
        if (!"".equals(userModel.getPhone())) {
            sql.append("AND phone LIKE '%").append(userModel.getPhone()).append("%'\n");
        }
        if (!"".equals(userModel.getIntroduce())) {
            sql.append("AND introduce LIKE '%").append(userModel.getIntroduce()).append("%'");
        }

        ResultSet resultSet = JDBC.getResultSet(sql.toString());
        int count = 0;

        try {
            while (resultSet.next()) {
                count = resultSet.getInt("count");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
}
