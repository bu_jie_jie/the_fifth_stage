package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.ClothingDao;
import com.cn.wanxi.model.ClothingModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClothingImpl implements ClothingDao {
    @Override
    public boolean findByName(String name) {
        String sql = "select * from clothing where name='" + name + "'";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            if (resultSet.next()) {
                isHave = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }

    @Override
    public int add(ClothingModel clothingModel) {
        String sql = "insert into clothing (name,is_show,create_time,update_time,is_top) values('" + clothingModel.getName() + "','" + clothingModel.getIsShow() + "','"+clothingModel.getCreateTime()+"','"+clothingModel.getUpdateTime()+"','"+clothingModel.getIsTop()+"');";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public List<ClothingModel> findAll() {
        String sql="select * from clothing";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<ClothingModel> list = new ArrayList<>();
        try {
            while (resultSet.next()){
                ClothingModel clothingModel=new ClothingModel();
                clothingModel.setId(resultSet.getInt("id"));
                clothingModel.setName(resultSet.getString("name"));
                clothingModel.setIsShow(resultSet.getInt("is_show"));
                clothingModel.setCreateTime(resultSet.getDate("create_time").toString());
                clothingModel.setUpdateTime(resultSet.getDate("update_time").toString());
                clothingModel.setIsTop(resultSet.getInt("is_top"));
                list.add(clothingModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from clothing where id=" + id;
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public ClothingModel findById(int i) {
        String sql = "SELECT * FROM clothing where id="+i;
        ResultSet resultSet=JDBC.getResultSet(sql);
        ClothingModel clothingModel=new ClothingModel();
        try {
            while (resultSet.next()){
                clothingModel.setName(resultSet.getString("name"));
                clothingModel.setIsShow(resultSet.getInt("is_show"));
                clothingModel.setIsTop(resultSet.getInt("is_top"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return clothingModel;
    }

    @Override
    public int edit(ClothingModel clothingModel) {
        String sql = "update clothing set name='" + clothingModel.getName() + "'," +
                "update_time='" + clothingModel.getUpdateTime() + "'," +
                "is_show=" + clothingModel.getIsShow() + "," +
                "is_top=" + clothingModel.getIsTop() + " " +
                "WHERE id=" + clothingModel.getId() + ";";
//        指示受影响的行数（即更新计数）
        return JDBC.excuteUpdate(sql);
    }
}
