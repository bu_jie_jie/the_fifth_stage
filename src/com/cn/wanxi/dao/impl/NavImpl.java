package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.NavDao;
import com.cn.wanxi.model.NavModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NavImpl implements NavDao {
    @Override
    public int add(NavModel navModel) {
        String sql = "insert into nav (name,src,creation_time,update_time,is_show,is_top) values ('" + navModel.getName() + "','" + navModel.getSrc() + "','" + navModel.getCreationTime() + "','" + navModel.getUpdateTime() + "'," + navModel.getIsShow() + "," + navModel.getIsTop() + ");";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public boolean findName(String name) {
        String sql = "select * FROM nav WHERE name='" + name + "';";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            isHave = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }

    @Override
    public List<NavModel> findAll() {
        String sql = "SELECT * FROM nav;";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<NavModel> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                NavModel navModel = new NavModel();
                navModel.setId(resultSet.getInt("id"));
                navModel.setName(resultSet.getString("name"));
                navModel.setSrc(resultSet.getString("src"));
                navModel.setCreationTime(resultSet.getString("creation_time"));
                navModel.setUpdateTime(resultSet.getString("update_time"));
                navModel.setIsShow(resultSet.getInt("is_show"));
                navModel.setIsTop(resultSet.getInt("is_top"));
                list.add(navModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from nav where id=" + id;
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public NavModel findById(int id) {
        String sql = "SELECT * FROM nav WHERE id=" + id;
        ResultSet resultSet = JDBC.getResultSet(sql);
        NavModel navModel = new NavModel();
        try {
            while (resultSet.next()) {
                navModel.setName(resultSet.getString("name"));
                navModel.setSrc(resultSet.getString("src"));
//                navModel.setCreationTime(resultSet.getString("creation_time"));
//                navModel.setUpdateTime(resultSet.getString("update_time"));
                navModel.setIsShow(resultSet.getInt("is_show"));
                navModel.setIsTop(resultSet.getInt("is_top"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return navModel;
    }

    @Override
    public int edit(NavModel navModel) {
        String sql = "update nav set name='" + navModel.getName() + "'," +
                "src='" + navModel.getSrc() + "'," +
                "update_time='" + navModel.getUpdateTime() + "'," +
                "is_show=" + navModel.getIsShow() + "," +
                "is_top=" + navModel.getIsTop() + " " +
                "WHERE id=" + navModel.getId() + ";";
//        指示受影响的行数（即更新计数）
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public List<NavModel> getNavModelForFront() {
        String sql = "SELECT * FROM nav WHERE is_show=1";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<NavModel> list=new ArrayList<>();
        try {
            while (resultSet.next()){
                NavModel navModel = new NavModel();
                navModel.setId(resultSet.getInt("id"));
                navModel.setName(resultSet.getString("name"));
                navModel.setSrc(resultSet.getString("src"));
                list.add(navModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


}
