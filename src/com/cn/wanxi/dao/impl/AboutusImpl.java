package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.AboutusDao;
import com.cn.wanxi.model.AboutusModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AboutusImpl implements AboutusDao {
    @Override
    public int add(AboutusModel aboutusModel) {
        String sql = "insert into aboutus" +
                "(title,contents,img_href,creation_time,update_time,is_show,is_top) " +
                "values ('" + aboutusModel.getTitle() + "'," +
                "'" + aboutusModel.getContents() + "'," +
                "'" + aboutusModel.getImgHref() + "'," +
                "'" + aboutusModel.getCreationTime() + "'," +
                "'" + aboutusModel.getUpdateTime() + "'," +
                "" + aboutusModel.getIsShow() + "," +
                "" + aboutusModel.getIsTop()  + ")";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public List<AboutusModel> findAll() {
        String sql = "select * FROM aboutus";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<AboutusModel> list=new ArrayList<>();
        try {
            while (resultSet.next()){
                AboutusModel aboutusModel=new AboutusModel();
                aboutusModel.setId(resultSet.getInt("id"));
                aboutusModel.setTitle(resultSet.getString("title"));
                aboutusModel.setContents(resultSet.getString("contents"));
                aboutusModel.setImgHref(resultSet.getString("img_href"));
                aboutusModel.setCreationTime(resultSet.getString("creation_time"));
                aboutusModel.setUpdateTime(resultSet.getString("update_time"));
                aboutusModel.setIsShow(resultSet.getInt("is_show"));
                aboutusModel.setIsTop(resultSet.getInt("is_top"));
                list.add(aboutusModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from aboutus where id=" + id;
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public AboutusModel findById(int id) {
        String sql = "select * FROM aboutus where id=" + id;
        ResultSet resultSet = JDBC.getResultSet(sql);
        AboutusModel aboutusModel=new AboutusModel();
        try {
            while (resultSet.next()){
                aboutusModel.setTitle(resultSet.getString("title"));
                aboutusModel.setContents(resultSet.getString("contents"));
                aboutusModel.setImgHref(resultSet.getString("img_href"));
                aboutusModel.setIsShow(resultSet.getInt("is_show"));
                aboutusModel.setIsTop(resultSet.getInt("is_top"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aboutusModel;
    }

    @Override
    public int edit(AboutusModel aboutusModel) {
        String sql = "update aboutus set title='" + aboutusModel.getTitle() + "'," +
                "contents='" + aboutusModel.getContents() + "'," +
                "img_href='" + aboutusModel.getImgHref() + "'," +
                "update_time='" + aboutusModel.getUpdateTime() + "'," +
                "is_show=" + aboutusModel.getIsShow() + "," +
                "is_top=" + aboutusModel.getIsTop() + " " +
                "WHERE id=" + aboutusModel.getId() + ";";
//        指示受影响的行数（即更新计数）
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public boolean findByTitle(String title) {
        String sql = "select * FROM aboutus WHERE title='" + title + "';";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            isHave = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }

    @Override
    public AboutusModel getAboutusModelForFront() {
        String sql="select * FROM aboutus where title='ad'";
        ResultSet resultSet=JDBC.getResultSet(sql);
        AboutusModel aboutusModel=null;
        try {
            while (resultSet.next()){
            aboutusModel=new AboutusModel();
            aboutusModel.setId(resultSet.getInt("id"));
            aboutusModel.setImgHref(resultSet.getString("img_href"));
            aboutusModel.setContents(resultSet.getString("contents"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return aboutusModel;
    }
}
