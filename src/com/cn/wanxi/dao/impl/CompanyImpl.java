package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.CompanyDao;
import com.cn.wanxi.model.CompanyModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CompanyImpl implements CompanyDao {
    @Override
    public int add(CompanyModel companyModel) {
        String sql = "insert into company (address,phone,qq,mail,creation_time,update_time,is_show,is_top) values ('" + companyModel.getAddress() + "','" + companyModel.getPhone() + "','" + companyModel.getQq() + "','"+ companyModel.getMail() + "','"+ companyModel.getCreationTime() + "','" + companyModel.getUpdateTime() + "'," + companyModel.getIsShow() + "," + companyModel.getIsTop() + ");";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public List<CompanyModel> findAll() {
        String sql = "SELECT * FROM company;";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<CompanyModel> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                CompanyModel companyModel = new CompanyModel();
                companyModel.setId(resultSet.getInt("id"));
                companyModel.setAddress(resultSet.getString("address"));
                companyModel.setPhone(resultSet.getString("phone"));
                companyModel.setQq(resultSet.getString("qq"));
                companyModel.setMail(resultSet.getString("mail"));
                companyModel.setCreationTime(resultSet.getString("creation_time"));
                companyModel.setUpdateTime(resultSet.getString("update_time"));
                companyModel.setIsShow(resultSet.getInt("is_show"));
                companyModel.setIsTop(resultSet.getInt("is_top"));
                list.add(companyModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from company where id=" + id;
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public CompanyModel findById(int id) {
        String sql = "SELECT * FROM company where id="+id;
        ResultSet resultSet=JDBC.getResultSet(sql);
        CompanyModel companyModel=new CompanyModel();
        try {
            while (resultSet.next()){
                companyModel.setAddress(resultSet.getString("address"));
                companyModel.setPhone(resultSet.getString("phone"));
                companyModel.setQq(resultSet.getString("qq"));
                companyModel.setMail(resultSet.getString("mail"));
                companyModel.setIsShow(resultSet.getInt("is_show"));
                companyModel.setIsTop(resultSet.getInt("is_top"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyModel;
    }

    @Override
    public int edit(CompanyModel companyModel) {
        String sql = "update company set address='" + companyModel.getAddress() + "'," +
                "phone='" + companyModel.getPhone() + "'," +
                "qq='" + companyModel.getQq() + "'," +
                "mail='" + companyModel.getMail() + "'," +
                "update_time='" + companyModel.getUpdateTime() + "'," +
                "is_show=" + companyModel.getIsShow() + "," +
                "is_top=" + companyModel.getIsTop() + " " +
                "WHERE id=" + companyModel.getId() + ";";
//        指示受影响的行数（即更新计数）
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public boolean findAddress(String address) {
        String sql = "select * FROM company WHERE address='" + address + "';";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            isHave = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }

    @Override
    public CompanyModel getCompanyModelForFront() {
        String sql = "select * FROM company WHERE is_show=1 and id=1;";
        ResultSet resultSet=JDBC.getResultSet(sql);
        CompanyModel companyModel=new CompanyModel();
        try {
            while (resultSet.next()){
                companyModel.setId(resultSet.getInt("id"));
                companyModel.setName(resultSet.getString("name"));
                companyModel.setContents(resultSet.getString("contents"));
                companyModel.setAddress(resultSet.getString("address"));
                companyModel.setPhone(resultSet.getString("phone"));
                companyModel.setMail(resultSet.getString("mail"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return companyModel;
    }
}
