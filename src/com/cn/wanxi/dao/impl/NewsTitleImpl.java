package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.NewsTitleDao;
import com.cn.wanxi.model.NewsTitleModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NewsTitleImpl implements NewsTitleDao {
    @Override
    public boolean findTitle(String title) {
        String sql = "select * FROM newstitle WHERE title='" + title + "';";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            isHave = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }

    @Override
    public int add(NewsTitleModel newsTitleModel) {
        String sql = "insert into newstitle (title,time,creation_time,update_time,is_show,is_top) values ('" + newsTitleModel.getTitle() + "','" + newsTitleModel.getTime() + "','" + newsTitleModel.getCreationTime() + "','" + newsTitleModel.getUpdateTime() + "'," + newsTitleModel.getIsShow() + "," + newsTitleModel.getIsTop() + ");";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public List<NewsTitleModel> findAll() {
        String sql = "SELECT * FROM newstitle;";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<NewsTitleModel> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                NewsTitleModel newsTitleModel = new NewsTitleModel();
                newsTitleModel.setId(resultSet.getInt("id"));
                newsTitleModel.setTitle(resultSet.getString("title"));
                newsTitleModel.setTime(resultSet.getString("time"));
                newsTitleModel.setCreationTime(resultSet.getString("creation_time"));
                newsTitleModel.setUpdateTime(resultSet.getString("update_time"));
                newsTitleModel.setIsShow(resultSet.getInt("is_show"));
                newsTitleModel.setIsTop(resultSet.getInt("is_top"));
                list.add(newsTitleModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from newstitle where id=" + id;
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public NewsTitleModel findById(int id) {
        String sql = "SELECT * FROM newstitle where id="+id;
        ResultSet resultSet=JDBC.getResultSet(sql);
        NewsTitleModel newsTitleModel=new NewsTitleModel();
        try {
            while (resultSet.next()){
                newsTitleModel.setTitle(resultSet.getString("title"));
                newsTitleModel.setTime(resultSet.getString("time"));
                newsTitleModel.setIsShow(resultSet.getInt("is_show"));
                newsTitleModel.setIsTop(resultSet.getInt("is_top"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return newsTitleModel;
    }

    @Override
    public int edit(NewsTitleModel newsTitleModel) {
        String sql = "update newstitle set title='" + newsTitleModel.getTitle() + "'," +
                "time='" + newsTitleModel.getTime() + "'," +
                "update_time='" + newsTitleModel.getUpdateTime() + "'," +
                "is_show=" + newsTitleModel.getIsShow() + "," +
                "is_top=" + newsTitleModel.getIsTop() + " " +
                "WHERE id=" + newsTitleModel.getId() + ";";
//        指示受影响的行数（即更新计数）
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public List<NewsTitleModel> getNewsTitleDao() {
        String sql = "SELECT * FROM newstitle where is_show = 1;";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<NewsTitleModel> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                NewsTitleModel newsTitleModel = new NewsTitleModel();
                newsTitleModel.setId(resultSet.getInt("id"));
                newsTitleModel.setTitle(resultSet.getString("title"));
                newsTitleModel.setTime(resultSet.getString("time"));
                newsTitleModel.setAdds(resultSet.getString("adds"));
                list.add(newsTitleModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
