package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.ContactusDao;
import com.cn.wanxi.model.ContactusModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ContactusImpl implements ContactusDao {
    @Override
    public List<ContactusModel> findAll() {
        String sql="select * from contactus";
        ResultSet resultSet= JDBC.getResultSet(sql);
        List<ContactusModel> list=new ArrayList<>();
        try {
            while (resultSet.next()){
                ContactusModel contactusModel=new ContactusModel();
                contactusModel.setId(resultSet.getInt("id"));
                contactusModel.setName(resultSet.getString("name"));
                contactusModel.setPhone(resultSet.getString("phone"));
                contactusModel.setContents(resultSet.getString("contents"));
                contactusModel.setHuifu(resultSet.getString("huifu"));
                contactusModel.setCreationTime(resultSet.getString("creation_time"));
                contactusModel.setUpdateTime(resultSet.getString("update_time"));
                contactusModel.setIsShow(resultSet.getInt("is_show"));
                contactusModel.setIsTop(resultSet.getInt("is_top"));
                list.add(contactusModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
