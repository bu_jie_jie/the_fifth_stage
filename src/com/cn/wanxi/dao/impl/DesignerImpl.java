package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.DesignerDao;
import com.cn.wanxi.model.DesignerModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DesignerImpl implements DesignerDao {
    @Override
    public int add(DesignerModel designerModel) {
        String sql = "insert into designer" +
                "(name,contents,img_href,creation_time,update_time,is_show,is_top) " +
                "values ('" + designerModel.getName() + "'," +
                "'" + designerModel.getContents() + "'," +
                "'" + designerModel.getImgHref() + "'," +
                "'" + designerModel.getCreationTime() + "'," +
                "'" + designerModel.getUpdateTime() + "'," +
                "" + designerModel.getIsShow() + "," +
                "" + designerModel.getIsTop()  + ")";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public List<DesignerModel> findAll() {
        String sql = "select * FROM designer";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<DesignerModel> list=new ArrayList<>();
        try {
            while (resultSet.next()){
                DesignerModel designerModel=new DesignerModel();
                designerModel.setId(resultSet.getInt("id"));
                designerModel.setName(resultSet.getString("name"));
                designerModel.setContents(resultSet.getString("contents"));
                designerModel.setImgHref(resultSet.getString("img_href"));
                designerModel.setCreationTime(resultSet.getString("creation_time"));
                designerModel.setUpdateTime(resultSet.getString("update_time"));
                designerModel.setIsShow(resultSet.getInt("is_show"));
                designerModel.setIsTop(resultSet.getInt("is_top"));
                list.add(designerModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from designer where id=" + id;
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public DesignerModel findById(int id) {
        String sql = "select * FROM designer where id=" + id;
        ResultSet resultSet = JDBC.getResultSet(sql);
        DesignerModel designerModel=new DesignerModel();
        try {
            while (resultSet.next()){
                designerModel.setName(resultSet.getString("name"));
                designerModel.setContents(resultSet.getString("contents"));
                designerModel.setImgHref(resultSet.getString("img_href"));
                designerModel.setIsShow(resultSet.getInt("is_show"));
                designerModel.setIsTop(resultSet.getInt("is_top"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return designerModel;
    }

    @Override
    public int edit(DesignerModel designerModel) {
        String sql = "update designer set name='" + designerModel.getName() + "'," +
                "contents='" + designerModel.getContents() + "'," +
                "img_href='" + designerModel.getImgHref() + "'," +
                "update_time='" + designerModel.getUpdateTime() + "'," +
                "is_show=" + designerModel.getIsShow() + "," +
                "is_top=" + designerModel.getIsTop() + " " +
                "WHERE id=" + designerModel.getId() + ";";
//        指示受影响的行数（即更新计数）
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public boolean findByName(String name) {
        String sql = "select * FROM designer WHERE name='" + name + "';";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            isHave = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }
}
