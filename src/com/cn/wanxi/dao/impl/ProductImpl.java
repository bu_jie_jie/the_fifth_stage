package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.ProductDao;
import com.cn.wanxi.model.ProductModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductImpl implements ProductDao {
    @Override
    public int add(ProductModel productModel) {
        String sql = "insert into product" +
                "(name,price,is_show,is_recommend,img_href,creation_time,update_time,clothing_id) " +
                "values ('" + productModel.getProductName() + "'," +
                "" + productModel.getPrice() + "," +
                "" + productModel.getIsShow() + "," +
                "" + productModel.getIsRecommend() + "," +
                "'" + productModel.getImgHref() + "'," +
                "'" + productModel.getCreationTime() + "'," +
                "'" + productModel.getUpdateTime() + "'," +
                "'" + productModel.getClothingId() + "')";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public boolean findByName(String name) {
        String sql = "select * from product where name='" + name + "'";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            while (resultSet.next()) {
                isHave = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }

    @Override
    public List<ProductModel> findAll() {
        String sql = "select p.*,c.name as clothing_name from product p,clothing c where p.clothing_id=c.id";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<ProductModel> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                ProductModel productModel = new ProductModel();
                productModel.setId(resultSet.getInt("id"));
                productModel.setProductName(resultSet.getString("name"));
                productModel.setPrice(resultSet.getFloat("price"));
                productModel.setIsShow(resultSet.getInt("is_show"));
                productModel.setIsRecommend(resultSet.getInt("is_recommend"));
                productModel.setImgHref(resultSet.getString("img_href"));
                productModel.setCreationTime(resultSet.getString("creation_time"));
                productModel.setUpdateTime(resultSet.getString("update_time"));
//                productModel.setClonthingId(resultSet.getInt("clothing_id"));
                productModel.setClothingName(resultSet.getString("clothing_name"));
                list.add(productModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from product where id=" + id;
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public ProductModel findById(int i) {
        String sql = "select * from product where id=" + i;
        ResultSet resultSet = JDBC.getResultSet(sql);
        ProductModel productModel = new ProductModel();
        try {
            while (resultSet.next()) {
                productModel.setId(resultSet.getInt("Id"));
                productModel.setProductName(resultSet.getString("name"));
                productModel.setPrice(resultSet.getFloat("price"));
                productModel.setIsShow(resultSet.getInt("is_show"));
                productModel.setIsRecommend(resultSet.getInt("is_recommend"));
                productModel.setImgHref(resultSet.getString("img_href"));
                productModel.setClonthingId(resultSet.getInt("clothing_id"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productModel;
    }

    @Override
    public int update(ProductModel productModel) {
        StringBuilder sql =new StringBuilder("update product set ");
        sql.append("name ='").append(productModel.getProductName()).append("',");
        sql.append("price='").append(productModel.getPrice()).append("',");
        sql.append("is_show='").append(productModel.getIsShow()).append("',");
        sql.append("is_recommend='").append(productModel.getIsRecommend()).append("',");
        sql.append("update_time='").append(productModel.getUpdateTime()).append("',");
        if (!"undefined".equals(productModel.getImgHref())) {
            sql.append("img_href='").append(productModel.getImgHref()).append("',");
        }
        sql.append("clothing_id='").append(productModel.getClothingId()).append("' ");
        sql.append("WHERE id='").append(productModel.getId()).append("';");

        return JDBC.excuteUpdate(sql.toString());
    }
}
