package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.CustomizeDao;
import com.cn.wanxi.model.CustomizeModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomizeImpl implements CustomizeDao {
    @Override
    public int add(CustomizeModel customizeModel) {
        String sql = "insert into customize" +
                "(title,contents,img_href,creation_time,update_time,is_show,is_top) " +
                "values ('" + customizeModel.getTitle() + "'," +
                "'" + customizeModel.getContents() + "'," +
                "'" + customizeModel.getImgHref() + "'," +
                "'" + customizeModel.getCreationTime() + "'," +
                "'" + customizeModel.getUpdateTime() + "'," +
                "" + customizeModel.getIsShow() + "," +
                "" + customizeModel.getIsTop()  + ")";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public boolean findByTitle(String title) {
        String sql = "select * FROM customize WHERE title='" + title + "';";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            isHave = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }

    @Override
    public List<CustomizeModel> findAll() {
        String sql = "select * FROM customize";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<CustomizeModel> list=new ArrayList<>();
        try {
            while (resultSet.next()){
                CustomizeModel customizeModel=new CustomizeModel();
                customizeModel.setId(resultSet.getInt("id"));
                customizeModel.setTitle(resultSet.getString("title"));
                customizeModel.setContents(resultSet.getString("contents"));
                customizeModel.setImgHref(resultSet.getString("img_href"));
                customizeModel.setCreationTime(resultSet.getString("creation_time"));
                customizeModel.setUpdateTime(resultSet.getString("update_time"));
                customizeModel.setIsShow(resultSet.getInt("is_show"));
                customizeModel.setIsTop(resultSet.getInt("is_top"));
                list.add(customizeModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from customize where id=" + id;
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public CustomizeModel findById(int id) {
        String sql = "select * FROM customize where id=" + id;
        ResultSet resultSet = JDBC.getResultSet(sql);
        CustomizeModel customizeModel=new CustomizeModel();
        try {
            while (resultSet.next()){
                customizeModel.setTitle(resultSet.getString("title"));
                customizeModel.setContents(resultSet.getString("contents"));
                customizeModel.setImgHref(resultSet.getString("img_href"));
                customizeModel.setIsShow(resultSet.getInt("is_show"));
                customizeModel.setIsTop(resultSet.getInt("is_top"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customizeModel;
    }

    @Override
    public int edit(CustomizeModel customizeModel) {
        String sql = "update customize set title='" + customizeModel.getTitle() + "'," +
                "contents='" + customizeModel.getContents() + "'," +
                "img_href='" + customizeModel.getImgHref() + "'," +
                "update_time='" + customizeModel.getUpdateTime() + "'," +
                "is_show=" + customizeModel.getIsShow() + "," +
                "is_top=" + customizeModel.getIsTop() + " " +
                "WHERE id=" + customizeModel.getId() + ";";
//        指示受影响的行数（即更新计数）
        return JDBC.excuteUpdate(sql);
    }
}
