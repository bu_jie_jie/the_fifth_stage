package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.WorkDao;
import com.cn.wanxi.model.WorkModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WorkImpl implements WorkDao {
    @Override
    public List<WorkModel> getNavModelForFront() {
        String sql = "SELECT * FROM work WHERE is_show=1";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<WorkModel> list=new ArrayList<>();
        try {
            while (resultSet.next()){
                WorkModel workModel = new WorkModel();
                workModel.setId(resultSet.getInt("id"));
                workModel.setImgHref(resultSet.getString("img_href"));
                workModel.setPictureHref(resultSet.getString("picture_href"));
                workModel.setContentMan(resultSet.getString("content_man"));
                workModel.setContentWoman(resultSet.getString("content_woman"));
                list.add(workModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
