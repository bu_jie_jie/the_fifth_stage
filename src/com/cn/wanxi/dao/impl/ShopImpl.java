package com.cn.wanxi.dao.impl;

import com.cn.wanxi.dao.ShopDao;
import com.cn.wanxi.model.ShopModel;
import com.cn.wanxi.util.JDBC;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ShopImpl implements ShopDao {
    @Override
    public int add(ShopModel shopModel) {
        String sql = "insert into shop" +
                "(name,address,img_href,creation_time,update_time,is_show,is_top) " +
                "values ('" + shopModel.getName() + "'," +
                "'" + shopModel.getAddress() + "'," +
                "'" + shopModel.getImgHref() + "'," +
                "'" + shopModel.getCreationTime() + "'," +
                "'" + shopModel.getUpdateTime() + "'," +
                "" + shopModel.getIsShow() + "," +
                "" + shopModel.getIsTop()  + ")";
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public List<ShopModel> findAll() {
        String sql = "select * FROM shop";
        ResultSet resultSet = JDBC.getResultSet(sql);
        List<ShopModel> list=new ArrayList<>();
        try {
            while (resultSet.next()){
                ShopModel shopModel=new ShopModel();
                shopModel.setId(resultSet.getInt("id"));
                shopModel.setName(resultSet.getString("name"));
                shopModel.setAddress(resultSet.getString("address"));
                shopModel.setImgHref(resultSet.getString("img_href"));
                shopModel.setCreationTime(resultSet.getString("creation_time"));
                shopModel.setUpdateTime(resultSet.getString("update_time"));
                shopModel.setIsShow(resultSet.getInt("is_show"));
                shopModel.setIsTop(resultSet.getInt("is_top"));
                list.add(shopModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int del(int id) {
        String sql = "delete from shop where id=" + id;
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public ShopModel findById(int id) {
        String sql = "select * FROM shop where id=" + id;
        ResultSet resultSet = JDBC.getResultSet(sql);
        ShopModel shopModel=new ShopModel();
        try {
            while (resultSet.next()){
                shopModel.setName(resultSet.getString("name"));
                shopModel.setAddress(resultSet.getString("address"));
                shopModel.setImgHref(resultSet.getString("img_href"));
                shopModel.setIsShow(resultSet.getInt("is_show"));
                shopModel.setIsTop(resultSet.getInt("is_top"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return shopModel;
    }

    @Override
    public int edit(ShopModel shopModel) {
        String sql = "update shop set name='" + shopModel.getName() + "'," +
                "address='" + shopModel.getAddress() + "'," +
                "img_href='" + shopModel.getImgHref() + "'," +
                "update_time='" + shopModel.getUpdateTime() + "'," +
                "is_show=" + shopModel.getIsShow() + "," +
                "is_top=" + shopModel.getIsTop() + " " +
                "WHERE id=" + shopModel.getId() + ";";
//        指示受影响的行数（即更新计数）
        return JDBC.excuteUpdate(sql);
    }

    @Override
    public boolean findByName(String name) {
        String sql = "select * FROM shop WHERE name='" + name + "';";
        ResultSet resultSet = JDBC.getResultSet(sql);
        boolean isHave = false;
        try {
            isHave = resultSet.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isHave;
    }

}
