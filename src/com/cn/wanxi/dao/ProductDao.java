package com.cn.wanxi.dao;

import com.cn.wanxi.model.ProductModel;

import java.util.List;

public interface ProductDao {
    int add(ProductModel productModel);

    boolean findByName(String name);

    List<ProductModel> findAll();

    int del(int id);

    ProductModel findById(int i);

    int update(ProductModel productModel);
}
