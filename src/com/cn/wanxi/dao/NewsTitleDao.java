package com.cn.wanxi.dao;

import com.cn.wanxi.model.NewsTitleModel;

import java.util.List;

public interface NewsTitleDao {
    boolean findTitle(String title);

    int add(NewsTitleModel newsTitleModel);

    List<NewsTitleModel> findAll();

    int del(int id);

    NewsTitleModel findById(int id);

    int edit(NewsTitleModel newsTitleModel);

    List<NewsTitleModel> getNewsTitleDao();
}
