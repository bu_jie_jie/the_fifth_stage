package com.cn.wanxi.dao;

import com.cn.wanxi.model.CustomizeModel;

import java.util.List;

public interface CustomizeDao {
    int add(CustomizeModel customizeModel);

    boolean findByTitle(String title);

    List<CustomizeModel> findAll();

    int del(int id);

    CustomizeModel findById(int id);

    int edit(CustomizeModel customizeModel);
}
