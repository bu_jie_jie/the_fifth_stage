package com.cn.wanxi.dao;

import com.cn.wanxi.model.WorkModel;

import java.util.List;

public interface WorkDao {
    List<WorkModel> getNavModelForFront();
}
