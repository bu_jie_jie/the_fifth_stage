package com.cn.wanxi.dao;

import com.cn.wanxi.model.ContactusModel;

import java.util.List;

public interface ContactusDao {
    List<ContactusModel> findAll();
}
