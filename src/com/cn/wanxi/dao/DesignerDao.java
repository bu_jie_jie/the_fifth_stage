package com.cn.wanxi.dao;

import com.cn.wanxi.model.DesignerModel;

import java.util.List;

public interface DesignerDao {
    int add(DesignerModel designerModel);

    List<DesignerModel> findAll();

    int del(int id);

    DesignerModel findById(int id);

    int edit(DesignerModel designerModel);

    boolean findByName(String name);
}
