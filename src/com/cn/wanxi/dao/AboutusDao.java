package com.cn.wanxi.dao;

import com.cn.wanxi.model.AboutusModel;

import java.util.List;

public interface AboutusDao {
    int add(AboutusModel aboutusModel);

    List<AboutusModel> findAll();

    int del(int id);

    AboutusModel findById(int id);

    int edit(AboutusModel aboutusModel);

    boolean findByTitle(String title);

    AboutusModel getAboutusModelForFront();
}
