package com.cn.wanxi.dao;

import com.cn.wanxi.model.UserModel;

import java.util.List;

public interface IUserDao {
    int add(UserModel userModel);
    boolean findByUsername(String username);
    boolean findByUsernameAndPassword(UserModel userModel);
    List<UserModel> findAll(UserModel userModel);
    int del(int id);
    UserModel findById(int id);
    int edit(UserModel userModel);
    int getCount(UserModel userModel);
}
