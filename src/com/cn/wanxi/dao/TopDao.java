package com.cn.wanxi.dao;

import com.cn.wanxi.model.TopModel;

public interface TopDao {
    TopModel getTopModelModelForFront();
}
