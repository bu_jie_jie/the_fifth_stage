package com.cn.wanxi.dao;

import com.cn.wanxi.model.NavModel;

import java.util.List;

public interface NavDao {
    int add(NavModel navModel);
    boolean findName(String name);

    List<NavModel> findAll();

    int del(int id);


    NavModel findById(int id);

    int edit(NavModel navModel);


    List<NavModel> getNavModelForFront();
}
