package com.cn.wanxi.dao;

import com.cn.wanxi.model.ShopModel;

import java.util.List;

public interface ShopDao {
    int add(ShopModel shopModel);

    List<ShopModel> findAll();

    int del(int id);

    ShopModel findById(int id);

    int edit(ShopModel shopModel);

    boolean findByName(String name);
}
