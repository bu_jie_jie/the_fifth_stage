package com.cn.wanxi.dao;

import com.cn.wanxi.model.CompanyModel;

import java.util.List;

public interface CompanyDao {
    int add(CompanyModel companyModel);

    List<CompanyModel> findAll();

    int del(int id);

    CompanyModel findById(int id);

    int edit(CompanyModel companyModel);

    boolean findAddress(String address);

    CompanyModel getCompanyModelForFront();
}
