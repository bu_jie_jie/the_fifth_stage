package com.cn.wanxi.dao;

import com.cn.wanxi.model.ClothingModel;

import java.util.List;

public interface ClothingDao {
    boolean findByName(String name);

    int add(ClothingModel clothingModel);
    List<ClothingModel> findAll();

    int del(int id);

    ClothingModel findById(int i);

    int edit(ClothingModel clothingModel);
}
