package com.cn.wanxi.servlet.nav;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.service.INavService;
import com.cn.wanxi.service.impl.NavServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/back/nav/del")
public class NavDelServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        Integer id= Integer.valueOf(req.getParameter("id"));
        //调用逻辑层
        INavService iNavService=new NavServiceImpl();
        int count = iNavService.del(id == null || "".equals(id) ? 0 : id);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("count", count);
        resp.getWriter().println(jsonObject);
    }
}
