package com.cn.wanxi.servlet.nav;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.NavModel;
import com.cn.wanxi.service.INavService;
import com.cn.wanxi.service.impl.NavServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/nav/edit")
public class NavEditServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //获取页面值
        Integer id= Integer.valueOf(req.getParameter("id"));
        String name=req.getParameter("name");
        String src=req.getParameter("src");
        Integer isShow= Integer.valueOf(req.getParameter("isShow"));
        Integer isTop= Integer.valueOf(req.getParameter("isTop"));
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));
        //封装
        NavModel navModel=new NavModel();
        navModel.setId(id == null || "".equals(id) ? 0 : id);
        navModel.setName(name);
        navModel.setSrc(src);
        navModel.setIsShow(isShow);
        navModel.setIsTop(isTop);
        navModel.setUpdateTime(updateTime);
        //调用逻辑层
        INavService iNavService=new NavServiceImpl();
        //得到返回数据
        int count=iNavService.edit(navModel);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("count", count);
        resp.getWriter().println(jsonObject);

    }
}
