package com.cn.wanxi.servlet.nav;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.NavModel;
import com.cn.wanxi.service.INavService;
import com.cn.wanxi.service.impl.NavServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/back/nav/findById")
public class NavFindByIdServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //获取id
        Integer id = Integer.valueOf(req.getParameter("id"));
        //调用逻辑层
        INavService navService = new NavServiceImpl();
        //接收查询结果
        NavModel navModel = navService.findByid(id);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("navModel", navModel);
        resp.getWriter().println(jsonObject);

    }
}
