package com.cn.wanxi.servlet.newsTitle;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.NewsTitleModel;
import com.cn.wanxi.service.INewsTitleService;
import com.cn.wanxi.service.impl.NewsTitleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/back/newsTitleEdit/findById")
public class NewsTitleFindByIdServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //获取页面值
        Integer id = Integer.valueOf(req.getParameter("id"));

        INewsTitleService iNewsTitleService=new NewsTitleServiceImpl();

        NewsTitleModel newsTitleModel=iNewsTitleService.findById("".equals(id) || id == null ? 0 : id);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("newsTitleModel", newsTitleModel);
        resp.getWriter().println(jsonObject);
    }
}
