package com.cn.wanxi.servlet.newsTitle;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.NewsTitleModel;
import com.cn.wanxi.service.INewsTitleService;
import com.cn.wanxi.service.impl.NewsTitleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/newsTitle/edit")
public class NewsTitleEditServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //获取页面值
        Integer id = Integer.valueOf(req.getParameter("id"));
        String title=req.getParameter("title");
        String time=req.getParameter("time");
        Integer isShow = Integer.valueOf(req.getParameter("isShow"));
        Integer isTop = Integer.valueOf(req.getParameter("isTop"));
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));

        //封装
        NewsTitleModel newsTitleModel=new NewsTitleModel();
        newsTitleModel.setId("".equals(id) || id == null ? 0 : id);
        newsTitleModel.setTitle(title);
        newsTitleModel.setTime(time);
        newsTitleModel.setIsShow(isShow);
        newsTitleModel.setIsTop(isTop);
        newsTitleModel.setUpdateTime(updateTime);
        INewsTitleService iNewsTitleServce=new NewsTitleServiceImpl();
        int count=iNewsTitleServce.edit(newsTitleModel);
        //返回值
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }
}
