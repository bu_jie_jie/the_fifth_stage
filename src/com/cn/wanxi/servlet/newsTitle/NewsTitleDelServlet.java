package com.cn.wanxi.servlet.newsTitle;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.service.INewsTitleService;
import com.cn.wanxi.service.impl.NewsTitleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/back/newsTitle/del")
public class NewsTitleDelServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //获取页面值
        Integer id = Integer.valueOf(req.getParameter("id"));

        INewsTitleService iNewsTitleServce = new NewsTitleServiceImpl();
        int count = iNewsTitleServce.del("".equals(id) || id == null ? 0 : id);
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }
}
