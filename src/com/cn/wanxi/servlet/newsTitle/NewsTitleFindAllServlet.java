package com.cn.wanxi.servlet.newsTitle;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.NewsTitleModel;
import com.cn.wanxi.service.INewsTitleService;
import com.cn.wanxi.service.impl.NewsTitleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/back/newsTitle/newsTitleFindAll")
public class NewsTitleFindAllServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        INewsTitleService iNewsTitleService=new NewsTitleServiceImpl();
        List<NewsTitleModel> list=iNewsTitleService.findAll();

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("list",list);
        resp.getWriter().println(jsonObject);
    }
}
