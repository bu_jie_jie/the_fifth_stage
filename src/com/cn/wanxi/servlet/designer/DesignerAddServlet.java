package com.cn.wanxi.servlet.designer;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.DesignerModel;
import com.cn.wanxi.service.IDesignerService;
import com.cn.wanxi.service.impl.DesignerServiceImpl;
import com.cn.wanxi.util.Upload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/designer/designerAdd")
public class DesignerAddServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        Upload upload = new Upload();
        String result = upload.imgUpload(req);
        String [] results=result.split("@@");

        DesignerModel designerModel=new DesignerModel();
        designerModel.setName(results[0]);
        designerModel.setContents(results[1]);
        designerModel.setImgHref(results[2]);
        designerModel.setIsShow(Integer.parseInt(results[3]));
        designerModel.setIsTop(Integer.parseInt(results[4]));

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));
        String creationTime=(simpleDateFormat.format(new Date()));
        designerModel.setCreationTime(creationTime);
        designerModel.setUpdateTime(updateTime);

        IDesignerService iShopService=new DesignerServiceImpl();
        int count=iShopService.add(designerModel);

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }

}
