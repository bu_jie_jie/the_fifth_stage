package com.cn.wanxi.servlet.designer;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.DesignerModel;
import com.cn.wanxi.service.IDesignerService;
import com.cn.wanxi.service.impl.DesignerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/back/designer/designerFindAll")
public class DesignerFindAllServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        IDesignerService iShopService=new DesignerServiceImpl();
        List<DesignerModel> list=iShopService.findAll();

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("list",list);
        resp.getWriter().println(jsonObject);
    }
}
