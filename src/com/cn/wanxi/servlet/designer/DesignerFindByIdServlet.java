package com.cn.wanxi.servlet.designer;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.DesignerModel;
import com.cn.wanxi.service.IDesignerService;
import com.cn.wanxi.service.impl.DesignerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/back/designer/designerFindById")
public class DesignerFindByIdServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        Integer id = Integer.valueOf(req.getParameter("id"));

        IDesignerService iShopService = new DesignerServiceImpl();
        DesignerModel designerModel = iShopService.findById("".equals(id) || id == null ? 0 : id);

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("designerModel",designerModel);
        resp.getWriter().println(jsonObject);
    }
}
