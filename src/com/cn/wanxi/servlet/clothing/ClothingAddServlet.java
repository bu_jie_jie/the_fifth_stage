package com.cn.wanxi.servlet.clothing;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.ClothingModel;
import com.cn.wanxi.service.ICothingService;
import com.cn.wanxi.service.impl.CothingServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/clothing/add")
public class ClothingAddServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        String name = req.getParameter("name");
        Integer isShow = Integer.valueOf(req.getParameter("isShow"));
        Integer isTop=Integer.valueOf(req.getParameter("isTop"));
        //封装
        ClothingModel clothingModel = new ClothingModel();
        clothingModel.setName(name);
        clothingModel.setIsShow("".equals(isShow) || isShow == null ? 2 : isShow);
        clothingModel.setIsTop("".equals(isTop) || isTop == null ? 2 : isTop);
        //获取当前时间
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        clothingModel.setCreateTime(simpleDateFormat.format(new Date()));
        clothingModel.setUpdateTime(simpleDateFormat.format(new Date()));
//        调用逻辑层
        ICothingService iCothingService = new CothingServiceImpl();
//        得到返回值
        int count = iCothingService.add(clothingModel);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("count", count);
        resp.getWriter().println(jsonObject);
    }
}
