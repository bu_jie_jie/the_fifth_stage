package com.cn.wanxi.servlet.clothing;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.ClothingModel;
import com.cn.wanxi.service.ICothingService;
import com.cn.wanxi.service.impl.CothingServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/back/clothing/clothingFindById")
public class ClothingFindByIdServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //获取页面值
        Integer id = Integer.valueOf(req.getParameter("id"));

        ICothingService iCothingService=new CothingServiceImpl();

        ClothingModel clothingModel=iCothingService.findById("".equals(id) || id == null ? 0 : id);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("clothingModel", clothingModel);
        resp.getWriter().println(jsonObject);
    }
}
