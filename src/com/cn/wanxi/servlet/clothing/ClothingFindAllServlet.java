package com.cn.wanxi.servlet.clothing;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.ClothingModel;
import com.cn.wanxi.service.ICothingService;
import com.cn.wanxi.service.impl.CothingServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/back/clothing/findAll")
public class ClothingFindAllServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //因是查询所有，所以不需要接收页面传值和封装
        //调用服务逻辑层
        ICothingService iCothingService=new CothingServiceImpl();
        List<ClothingModel> list=iCothingService.findAll();
        // 返回json（页面)
        JSONObject jsonObject = new JSONObject();
        //将整型转换为json对象
        jsonObject.put("list", list);
        //传递到页面的值
        resp.getWriter().println(jsonObject);
    }
}
