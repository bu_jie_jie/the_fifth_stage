package com.cn.wanxi.servlet.clothing;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.ClothingModel;
import com.cn.wanxi.service.ICothingService;
import com.cn.wanxi.service.impl.CothingServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/clothing/clothingEdit")
public class ClothingEditServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //获取页面值
        Integer id = Integer.valueOf(req.getParameter("id"));
        String name=req.getParameter("name");
        Integer isShow = Integer.valueOf(req.getParameter("isShow"));
        Integer isTop = Integer.valueOf(req.getParameter("isTop"));
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));

        //封装
        ClothingModel clothingModel=new ClothingModel();
        clothingModel.setId("".equals(id) || id == null ? 0 : id);
        clothingModel.setName(name);
        clothingModel.setIsShow(isShow);
        clothingModel.setIsTop(isTop);
        clothingModel.setUpdateTime(updateTime);
        ICothingService iCothingService=new CothingServiceImpl();
        int count=iCothingService.edit(clothingModel);
        //返回值
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);

    }

}
