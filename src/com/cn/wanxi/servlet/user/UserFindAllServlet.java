package com.cn.wanxi.servlet.user;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.PageModel;
import com.cn.wanxi.model.UserModel;
import com.cn.wanxi.service.IUserService;
import com.cn.wanxi.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/back/user/findAll")
public class UserFindAllServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //拿前端传过来的值

        String userName = req.getParameter("userName");
        String startBirthday = req.getParameter("startBirthday");
        String endBirthday = req.getParameter("endBirthday");
        String introduce = req.getParameter("introduce");
        String sex = req.getParameter("sex");
        String phone = req.getParameter("phone");
        String pageNum = req.getParameter("pageNum");
        String pageSize = req.getParameter("pageSize");

        //封装
        UserModel userModel = new UserModel();
        userModel.setUsername(userName);
        userModel.setStartBirthday(startBirthday);
        userModel.setEndBirthday(endBirthday);
        userModel.setIntroduce(introduce);
        userModel.setSex(sex);
        userModel.setPhone(phone);
        PageModel pageModel = new PageModel();
        pageModel.setPageNum(pageNum == null || "".equals(pageNum) ? 1 : Integer.parseInt(pageNum));
        pageModel.setPageSize(pageSize == null || "".equals(pageSize) ? 7 : Integer.parseInt(pageSize));
        userModel.setPageModel(pageModel);
        //调用服务逻辑层
        IUserService iUserService = new UserServiceImpl();
        //接受返回值
        List<UserModel> list = iUserService.findAll(userModel);
        int count=iUserService.getCount(userModel);
        // 返回json（页面)
        JSONObject jsonObject = new JSONObject();
        //将整型转换为json对象
        jsonObject.put("list", list);
        jsonObject.put("count",count);
        //传递到页面的值
        resp.getWriter().println(jsonObject);

    }
}
