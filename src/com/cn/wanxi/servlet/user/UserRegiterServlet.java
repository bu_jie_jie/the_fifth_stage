package com.cn.wanxi.servlet.user;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.UserModel;
import com.cn.wanxi.service.IUserService;
import com.cn.wanxi.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/UserRegiter")
public class UserRegiterServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        String sex=req.getParameter("sex");
        String birthday=req.getParameter("birthday");
        String phone=req.getParameter("phone");
        String introduce=req.getParameter("introduce");

        //封装进model
        UserModel userModel=new UserModel();
        userModel.setUsername(username);
        userModel.setPassword(password);
        userModel.setSex(sex);
        userModel.setBirthday(birthday);
        userModel.setPhone(phone);
        userModel.setIntroduce(introduce);
        //调用逻辑层
        IUserService iUserService=new UserServiceImpl();
        //得到返回数据
        int count=iUserService.add(userModel);
//        当页面使用ajax进行消息传递时，不可用sendReadirect进行页面跳转
//        if (count==1){
//            resp.sendRedirect("/back/UserRegiter.html");
//        }else {
//            req.setAttribute("result","失败");
//            resp.sendRedirect("/back/UserRegiter.jsp");
//        }
        JSONObject jsonObject=new JSONObject();
//        将整型转换为json对象
        jsonObject.put("count",count);
        //jsonObject.put("i",i);
        //将值传递到页面
        resp.getWriter().println(jsonObject);
    }
}
