package com.cn.wanxi.servlet.user;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.UserModel;
import com.cn.wanxi.service.IUserService;
import com.cn.wanxi.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        String username=req.getParameter("username");
        String password=req.getParameter("password");
        String code=req.getParameter("code");
        Object code1=req.getSession().getAttribute("code");
        //封装进model
        UserModel userModel=new UserModel();
        userModel.setUsername(username);
        userModel.setPassword(password);
        userModel.setCode(code);
        userModel.setCode1((String) code1);
        //调用逻辑层
        IUserService iUserService=new UserServiceImpl();
        //得到返回值
        int count=iUserService.login(userModel);
        JSONObject jsonObject=new JSONObject();
        if (count==1){
            req.getSession().setAttribute("username",username);
        }
//        将整型转换为json对象
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }
}
