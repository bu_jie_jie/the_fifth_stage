package com.cn.wanxi.servlet.user;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.UserModel;
import com.cn.wanxi.service.IUserService;
import com.cn.wanxi.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/user/edit")
public class UserEditServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        Integer id = Integer.valueOf(req.getParameter("id"));
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String sex = req.getParameter("sex");
        String birthday = req.getParameter("birthday");
        String phone = req.getParameter("phone");
        String introduce = req.getParameter("introduce");
        //获取当前时间
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime = (simpleDateFormat.format(new Date()));
        //String creationTime = req.getParameter("creationTime");
        Integer isShow = Integer.valueOf(req.getParameter("isShow"));
        Integer isTop = Integer.valueOf(req.getParameter("isTop"));
        //封装进model
        UserModel userModel = new UserModel();
        userModel.setId(id);
        userModel.setUsername(username);
        userModel.setPassword(password);
        userModel.setSex(sex);
        userModel.setBirthday(birthday);
        userModel.setPhone(phone);
        userModel.setIntroduce(introduce);
//        userModel.setCreationTime(creationTime);
        userModel.setUpdateTime(updateTime);
        userModel.setIsShow(isShow);
        userModel.setIsTop(isTop);

        //调用逻辑层
        IUserService iUserService = new UserServiceImpl();
        //得到返回数据
        int count = iUserService.edit(userModel);

        JSONObject jsonObject = new JSONObject();
//        将整型转换为json对象
        jsonObject.put("count", count);
        //jsonObject.put("i",i);
        //将值传递到页面
        resp.getWriter().println(jsonObject);
    }
}
