package com.cn.wanxi.servlet.user;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.UserModel;
import com.cn.wanxi.service.IUserService;
import com.cn.wanxi.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/back/user/findById")
public class UserFindByIdServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        String idString = req.getParameter("id");
        //调用服务逻辑层
        int id = idString == null || "".equals(idString) ? 0 : Integer.parseInt(idString);
        IUserService iUserService = new UserServiceImpl();
        //接受返回值
        UserModel userModel = iUserService.findById(id);
        // 返回json（页面)
        JSONObject jsonObject = new JSONObject();
        //将整型转换为json对象
        jsonObject.put("userModel", userModel);
        //传递到页面的值
        resp.getWriter().println(jsonObject);
    }
}
