package com.cn.wanxi.servlet.shop;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.ShopModel;
import com.cn.wanxi.service.IShopService;
import com.cn.wanxi.service.impl.ShopServiceImpl;
import com.cn.wanxi.util.Upload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/shop/shopAdd")
public class ShopAddServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        Upload upload = new Upload();
        String result = upload.imgUpload(req);
        String [] results=result.split("@@");

        ShopModel shopModel=new ShopModel();
        shopModel.setName(results[0]);
        shopModel.setAddress(results[1]);
        shopModel.setImgHref(results[2]);
        shopModel.setIsShow(Integer.parseInt(results[3]));
        shopModel.setIsTop(Integer.parseInt(results[4]));

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));
        String creationTime=(simpleDateFormat.format(new Date()));
        shopModel.setCreationTime(creationTime);
        shopModel.setUpdateTime(updateTime);

        IShopService iShopService=new ShopServiceImpl();
        int count=iShopService.add(shopModel);

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }
}
