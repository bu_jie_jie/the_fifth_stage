package com.cn.wanxi.servlet.shop;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.ShopModel;
import com.cn.wanxi.service.IShopService;
import com.cn.wanxi.service.impl.ShopServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/back/shop/shopFindAll")
public class ShopFindAllServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        IShopService iShopService=new ShopServiceImpl();
        List<ShopModel> list=iShopService.findAll();

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("list",list);
        resp.getWriter().println(jsonObject);
    }
}
