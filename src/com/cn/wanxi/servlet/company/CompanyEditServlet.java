package com.cn.wanxi.servlet.company;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.CompanyModel;
import com.cn.wanxi.service.ICompanyService;
import com.cn.wanxi.service.impl.CompanyServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/company/companyEdit")
public class CompanyEditServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //获取页面值
        Integer id = Integer.valueOf(req.getParameter("id"));
        String address=req.getParameter("address");
        String phone=req.getParameter("phone");
        String qq=req.getParameter("qq");
        String mail=req.getParameter("mail");
        Integer isShow = Integer.valueOf(req.getParameter("isShow"));
        Integer isTop = Integer.valueOf(req.getParameter("isTop"));
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));

        //封装
        CompanyModel companyModel=new CompanyModel();
        companyModel.setId("".equals(id) || id == null ? 0 : id);
        companyModel.setAddress(address);
        companyModel.setPhone(phone);
        companyModel.setQq(qq);
        companyModel.setMail(mail);
        companyModel.setIsShow(isShow);
        companyModel.setIsTop(isTop);
        companyModel.setUpdateTime(updateTime);
        ICompanyService iCompanyService=new CompanyServiceImpl();
        int count=iCompanyService.edit(companyModel);
        //返回值
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }
}
