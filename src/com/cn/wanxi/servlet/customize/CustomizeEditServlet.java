package com.cn.wanxi.servlet.customize;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.CustomizeModel;
import com.cn.wanxi.service.ICustomizeService;
import com.cn.wanxi.service.impl.CustomizeServiceImpl;
import com.cn.wanxi.util.Upload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/customize/edit")
public class CustomizeEditServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        Upload upload = new Upload();
        String result = upload.imgUpload(req);
        String [] results=result.split("@@");
        CustomizeModel customizeModel=new CustomizeModel();
        customizeModel.setTitle(results[0]);
        customizeModel.setContents(results[1]);
        customizeModel.setImgHref(results[2]);
        customizeModel.setIsShow(Integer.parseInt(results[3]));
        customizeModel.setIsTop(Integer.parseInt(results[4]));
        customizeModel.setId(Integer.parseInt(results[5]));
        //放入更新時間
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));
        customizeModel.setUpdateTime(updateTime);

        ICustomizeService iCustomizeService=new CustomizeServiceImpl();
        int count=iCustomizeService.edit(customizeModel);

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }
}
