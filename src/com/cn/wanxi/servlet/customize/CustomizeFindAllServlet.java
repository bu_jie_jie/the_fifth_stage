package com.cn.wanxi.servlet.customize;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.CustomizeModel;
import com.cn.wanxi.service.ICustomizeService;
import com.cn.wanxi.service.impl.CustomizeServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/back/customize/findAll")
public class CustomizeFindAllServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        ICustomizeService iCustomizeService=new CustomizeServiceImpl();
        List<CustomizeModel> list=iCustomizeService.findAll();

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("list",list);
        resp.getWriter().println(jsonObject);
    }
}
