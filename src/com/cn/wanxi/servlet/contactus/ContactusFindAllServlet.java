package com.cn.wanxi.servlet.contactus;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.ContactusModel;
import com.cn.wanxi.service.IContactusService;
import com.cn.wanxi.service.impl.ContactusServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/back/contactus/contactusFindAll")
public class ContactusFindAllServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        IContactusService iCompanyService=new ContactusServiceImpl();
        List<ContactusModel> list=iCompanyService.findAll();

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("list",list);
        resp.getWriter().println(jsonObject);
    }
}
