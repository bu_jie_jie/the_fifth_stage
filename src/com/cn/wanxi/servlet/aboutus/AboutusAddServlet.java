package com.cn.wanxi.servlet.aboutus;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.AboutusModel;
import com.cn.wanxi.service.IAboutusService;
import com.cn.wanxi.service.impl.AboutusServiceImpl;
import com.cn.wanxi.util.Upload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/aboutus/add")
public class AboutusAddServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");
        //接收页面传值
        Upload upload = new Upload();
        String result = upload.imgUpload(req);
        String [] results=result.split("@@");

        AboutusModel aboutusModel=new AboutusModel();
        aboutusModel.setTitle(results[0]);
        aboutusModel.setContents(results[1]);
        aboutusModel.setImgHref(results[2]);
        aboutusModel.setIsShow(Integer.parseInt(results[3]));
        aboutusModel.setIsTop(Integer.parseInt(results[4]));

        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));
        String creationTime=(simpleDateFormat.format(new Date()));
        aboutusModel.setCreationTime(creationTime);
        aboutusModel.setUpdateTime(updateTime);

        IAboutusService iAboutusService=new AboutusServiceImpl();
        int count=iAboutusService.add(aboutusModel);

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }
}
