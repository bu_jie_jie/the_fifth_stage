package com.cn.wanxi.servlet.aboutus;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.AboutusModel;
import com.cn.wanxi.service.IAboutusService;
import com.cn.wanxi.service.impl.AboutusServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/back/aboutus/findById")
public class AboutusFindByIdServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //乱码处理
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        Integer id = Integer.valueOf(req.getParameter("id"));

        IAboutusService iAboutusService = new AboutusServiceImpl();
        AboutusModel aboutusModel = iAboutusService.findById("".equals(id) || id == null ? 0 : id);

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("aboutusModel",aboutusModel);
        resp.getWriter().println(jsonObject);

    }
}
