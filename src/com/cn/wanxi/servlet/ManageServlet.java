package com.cn.wanxi.servlet;

import com.alibaba.fastjson.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/manage")
public class ManageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       Object username=req.getSession().getAttribute("username");
        JSONObject jsonObject=new JSONObject();
        if (username!=null){
            jsonObject.put("username",username);

        }else {
            jsonObject.put("username",null);

        }
        resp.getWriter().println(jsonObject);
    }
}
