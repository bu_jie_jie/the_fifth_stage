package com.cn.wanxi.servlet.product;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.ProductModel;
import com.cn.wanxi.service.IProductService;
import com.cn.wanxi.service.impl.ProductServiceImpl;
import com.cn.wanxi.util.Upload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/product/update")
public class ProductUpdate extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        Upload upload = new Upload();
        String result = upload.imgUpload(req);
        //接收页面传值

        String [] results=result.split("@@");
//        String productName = req.getParameter("productName");

        ProductModel productModel=new ProductModel();
        productModel.setClonthingId(Integer.parseInt(results[0]));
        productModel.setProductName("".equals(results[1])?"默认":results[1]);
        productModel.setPrice("".equals(results[2])?  0.0f :Float.parseFloat(results[2]));
        productModel.setIsShow(Integer.parseInt(results[3]));
        productModel.setIsRecommend(Integer.parseInt(results[4]));
        productModel.setImgHref(results[5]);
        productModel.setId(Integer.parseInt(results[6]));
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));
        productModel.setUpdateTime(updateTime);

        IProductService iProductService=new ProductServiceImpl();
        int count=iProductService.update(productModel);

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }
}
