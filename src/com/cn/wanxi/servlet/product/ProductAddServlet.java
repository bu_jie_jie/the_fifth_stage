package com.cn.wanxi.servlet.product;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.model.ProductModel;
import com.cn.wanxi.service.IProductService;
import com.cn.wanxi.service.impl.ProductServiceImpl;
import com.cn.wanxi.util.Upload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/back/product/add")
public class ProductAddServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        //接收页面传值
        Upload upload = new Upload();
        String result = upload.imgUpload(req);
        String [] results=result.split("@@");
//        String productName = req.getParameter("productName");
//        1@@燕尾服@@2222@@是@@否@@/loadImg/3.jpg@@
        ProductModel productModel=new ProductModel();
        productModel.setClonthingId(Integer.parseInt(results[0]));
        productModel.setProductName(results[1]);
        productModel.setPrice(Float.parseFloat(results[2]));
        productModel.setIsShow(Integer.parseInt(results[3]));
        productModel.setIsRecommend(Integer.parseInt(results[4]));
        productModel.setImgHref(results[5]);
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        String updateTime=(simpleDateFormat.format(new Date()));
        String creationTime=(simpleDateFormat.format(new Date()));
        productModel.setCreationTime(creationTime);
        productModel.setUpdateTime(updateTime);

        IProductService iProductService=new ProductServiceImpl();
        int count=iProductService.add(productModel);

        JSONObject jsonObject=new JSONObject();
        jsonObject.put("count",count);
        resp.getWriter().println(jsonObject);
    }
}
