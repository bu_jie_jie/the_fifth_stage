package com.cn.wanxi.servlet.product;

import com.alibaba.fastjson.JSONObject;
import com.cn.wanxi.service.IProductService;
import com.cn.wanxi.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/back/product/del")
public class ProductDelServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("application/json; charset=utf-8");

        Integer id = Integer.valueOf(req.getParameter("id"));

        IProductService iProductService = new ProductServiceImpl();

        int count = iProductService.del(id == null || "".equals(id) ? 0 : id);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("count", count);
        resp.getWriter().println(jsonObject);
    }
}
