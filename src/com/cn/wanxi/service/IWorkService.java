package com.cn.wanxi.service;

import com.cn.wanxi.model.WorkModel;

import java.util.List;

public interface IWorkService {
    List<WorkModel> getWorkList();
}
