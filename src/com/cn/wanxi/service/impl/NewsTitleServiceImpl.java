package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.NewsTitleDao;
import com.cn.wanxi.dao.impl.NewsTitleImpl;
import com.cn.wanxi.model.NewsTitleModel;
import com.cn.wanxi.service.INewsTitleService;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class NewsTitleServiceImpl implements INewsTitleService {
    @Override
    public int add(NewsTitleModel newsTitleModel) {
        if (findTitle(newsTitleModel.getTitle())){
            return -2;
        }else {
            NewsTitleDao newsTitle=new NewsTitleImpl();
            return newsTitle.add(newsTitleModel);
        }
    }

    @Override
    public List<NewsTitleModel> findAll() {
        NewsTitleDao newsTitleDao=new NewsTitleImpl();
        return newsTitleDao.findAll();
    }

    @Override
    public int del(int id) {
        NewsTitleDao newsTitleDao=new NewsTitleImpl();
        return newsTitleDao.del(id);
    }

    @Override
    public NewsTitleModel findById(int id) {
        NewsTitleDao newsTitleDao=new NewsTitleImpl();
        return newsTitleDao.findById(id);
    }

    @Override
    public int edit(NewsTitleModel newsTitleModel) {
        NewsTitleDao newsTitleDao=new NewsTitleImpl();
        return newsTitleDao.edit(newsTitleModel);
    }

    @Override
    public List<NewsTitleModel> getnewsTitleModels() {
        Jedis jedis=new Jedis();
        List<NewsTitleModel> list=new ArrayList<>();
        long length=jedis.llen("newsTitleId");
        if (length>0){
            List<String> newsTitleAdds=jedis.lrange("newsTitleAdds",0,length);
            List<String> newsTitleTitle=jedis.lrange("newsTitleTitle",0,length);
            List<String> newsTitleTime=jedis.lrange("newsTitleTime",0,length);
            for (int i=0 ;i<length;i++){
                NewsTitleModel newsTitleModel=new NewsTitleModel();
                newsTitleModel.setAdds(newsTitleAdds.get(i));
                newsTitleModel.setTitle(newsTitleTitle.get(i));
                newsTitleModel.setTime(newsTitleTime.get(i));
                list.add(newsTitleModel);
            }
        }else {
            NewsTitleDao newsTitleDao=new NewsTitleImpl();
            list=newsTitleDao.getNewsTitleDao();
            for (NewsTitleModel newsTitleModel:list){
                jedis.rpush("newsTitleId", String.valueOf(newsTitleModel.getId()));
                jedis.rpush("newsTitleAdds",newsTitleModel.getAdds());
                jedis.rpush("newsTitleTitle",newsTitleModel.getTitle());
                jedis.rpush("newsTitleTime",newsTitleModel.getTime());
            }
        }
        return list;
    }

    private boolean findTitle(String title) {
        NewsTitleDao newsTitle=new NewsTitleImpl();
        return newsTitle.findTitle(title);
    }
}
