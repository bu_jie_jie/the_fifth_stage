package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.DesignerDao;
import com.cn.wanxi.dao.impl.DesignerImpl;
import com.cn.wanxi.model.DesignerModel;
import com.cn.wanxi.service.IDesignerService;

import java.util.List;

public class DesignerServiceImpl implements IDesignerService {
    @Override
    public int add(DesignerModel designerModel) {
        if (findByName(designerModel.getName())){
            return -2;
        }else {
            DesignerDao designerDao=new DesignerImpl();
            return designerDao.add(designerModel);
        }
    }

    public List<DesignerModel> findAll() {
        DesignerDao designerDao=new DesignerImpl();
        return designerDao.findAll();
    }


    public int del(int id) {
        DesignerDao designerDao=new DesignerImpl();
        return designerDao.del(id);
    }


    public DesignerModel findById(int id) {
        DesignerDao designerDao=new DesignerImpl();
        return designerDao.findById(id);
    }


    public int edit(DesignerModel designerModel) {
        DesignerDao designerDao=new DesignerImpl();
        return designerDao.edit(designerModel);
    }
    private boolean findByName(String name) {
        DesignerDao designerDao=new DesignerImpl();
        return designerDao.findByName(name);
    }
}
