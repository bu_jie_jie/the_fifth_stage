package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.CompanyDao;
import com.cn.wanxi.dao.impl.CompanyImpl;
import com.cn.wanxi.model.CompanyModel;
import com.cn.wanxi.service.ICompanyService;
import redis.clients.jedis.Jedis;

import java.util.List;

public class CompanyServiceImpl implements ICompanyService {

    public int add(CompanyModel companyModel) {
        if (findAddress(companyModel.getAddress())) {
            return -2;
        } else {
            CompanyDao companyDao = new CompanyImpl();
            return companyDao.add(companyModel);
        }
    }


    public List<CompanyModel> findAll() {
        CompanyDao companyDao = new CompanyImpl();
        return companyDao.findAll();
    }


    public int del(int id) {
        CompanyDao companyDao = new CompanyImpl();
        return companyDao.del(id);
    }


    public CompanyModel findById(int id) {
        CompanyDao companyDao = new CompanyImpl();
        return companyDao.findById(id);
    }


    public int edit(CompanyModel companyModel) {
        CompanyDao companyDao = new CompanyImpl();
        return companyDao.edit(companyModel);
    }

    @Override
    public CompanyModel getCompanyModel() {
        CompanyModel companyModel = null;
        Jedis jedis = new Jedis();
        //首先判断缓存是否有值
        if (jedis.exists("CompanyId")) {
            companyModel = new CompanyModel();
            companyModel.setName(jedis.get("CompanyName"));
            companyModel.setContents(jedis.get("CompanyContents"));
            companyModel.setAddress(jedis.get("CompanyAddress"));
            companyModel.setPhone(jedis.get("CompanyPhone"));
            companyModel.setMail(jedis.get("CompanyMail"));
        } else {
            CompanyDao companyDao = new CompanyImpl();
            companyModel = companyDao.getCompanyModelForFront();
            //先将id 存入缓存 再将 缓存清空
            jedis.setex("CompanyId", 60 * 30, companyModel.getId().toString());
            jedis.set("CompanyName", companyModel.getName());
            jedis.set("CompanyContents", companyModel.getContents());
            jedis.set("CompanyAddress", companyModel.getAddress());
            jedis.set("CompanyPhone", companyModel.getPhone());
            jedis.set("CompanyMail", companyModel.getMail());
        }
        return companyModel;
    }

    private boolean findAddress(String address) {
        CompanyDao newsTitle = new CompanyImpl();
        return newsTitle.findAddress(address);
    }
}
