package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.ProductDao;
import com.cn.wanxi.dao.impl.ProductImpl;
import com.cn.wanxi.model.ProductModel;
import com.cn.wanxi.service.IProductService;

import java.util.List;

public class ProductServiceImpl implements IProductService {
    @Override
    public int add(ProductModel productModel) {
        ProductDao productDao = new ProductImpl();
        int result = 0;
        if (findByName(productModel.getProductName())) {
            result = 2;
        } else {
            result = productDao.add(productModel);
        }
        return result;
    }

    @Override
    public List<ProductModel> findAll() {
        ProductDao productDao = new ProductImpl();
        return productDao.findAll();
    }

    @Override
    public int del(int id) {
        ProductDao productDao=new ProductImpl();

        return productDao.del(id);
    }

    @Override
    public ProductModel findById(int i) {
        ProductDao productDao=new ProductImpl();
        return productDao.findById(i);
    }

    @Override
    public int update(ProductModel productModel) {
        ProductDao productDao=new ProductImpl();
        return productDao.update(productModel);
    }

    private boolean findByName(String name) {
        ProductDao productDao = new ProductImpl();
        return productDao.findByName(name);
    }
}
