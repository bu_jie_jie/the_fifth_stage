package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.TopDao;
import com.cn.wanxi.dao.impl.TopImpl;
import com.cn.wanxi.model.TopModel;
import com.cn.wanxi.service.ITopService;
import redis.clients.jedis.Jedis;

public class TopServiceImpl implements ITopService {
    @Override
    public TopModel getTopModel() {
        TopModel topModel = null;
        Jedis jedis = new Jedis();
        //首先判断缓存是否有值
        if (jedis.exists("topId")) {
            topModel=new TopModel();
            topModel.setImgHref(jedis.get("topImg_href"));
            topModel.setContents(jedis.get("topContents"));
        } else {
            TopDao aboutusDao = new TopImpl();
            topModel = aboutusDao.getTopModelModelForFront();
            //先将id 存入缓存 再将 缓存清空
            jedis.setex("topId",60*30,topModel.getId().toString());
            jedis.set("topImg_href",topModel.getImgHref());
            jedis.set("topContents",topModel.getContents());
        }
        return topModel;
    }
}
