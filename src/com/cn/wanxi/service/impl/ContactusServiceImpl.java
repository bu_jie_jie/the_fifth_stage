package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.ContactusDao;
import com.cn.wanxi.dao.impl.ContactusImpl;
import com.cn.wanxi.model.ContactusModel;
import com.cn.wanxi.service.IContactusService;

import java.util.List;

public class ContactusServiceImpl implements IContactusService {
    @Override
    public List<ContactusModel> findAll() {
        ContactusDao contactusDao=new ContactusImpl();
        return contactusDao.findAll();
    }
}
