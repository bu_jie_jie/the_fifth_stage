package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.NavDao;
import com.cn.wanxi.dao.impl.NavImpl;
import com.cn.wanxi.model.NavModel;
import com.cn.wanxi.service.INavService;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class NavServiceImpl implements INavService {
    @Override
    public int add(NavModel navModel) {
        if (findName(navModel.getName())){
            return -2;
        }else {
            NavDao navDao=new NavImpl();
            return navDao.add(navModel);
        }
    }

    @Override
    public List<NavModel> findAll() {
        NavDao navDao=new NavImpl();
        return navDao.findAll();
    }

    @Override
    public int del(int id) {
        NavDao navDao=new NavImpl();
        return navDao.del(id);
    }

    @Override
    public NavModel findByid(Integer id) {
      NavDao navDao=new NavImpl();
      return navDao.findById(id);
    }

    @Override
    public int edit(NavModel navModel) {
        NavDao navDao=new NavImpl();
        return navDao.edit(navModel);
    }

    @Override
    public List<NavModel> getNavList() {
        Jedis jedis=new Jedis();
        List<NavModel> list=new ArrayList<>();
        long length=jedis.llen("NavName");
        if (length>0){
            List<String> name=jedis.lrange("NavName",0,length);
            List<String> src=jedis.lrange("NavSrc",0,length);
            for (int i = 0;i<length;i++){
                NavModel navModel=new NavModel();
                navModel.setName(name.get(i));
                navModel.setSrc(src.get(i));
                list.add(navModel);
            }
        }else {
            NavDao navDao=new NavImpl();
            list=navDao.getNavModelForFront();
            for (NavModel model: list) {
                jedis.rpush("NavName",model.getName());
                jedis.rpush("NavSrc",model.getSrc());
            }
        }
        return list ;

    }


    public boolean findName(String name){
        NavDao navDao=new NavImpl();
        return navDao.findName(name);
    }
}
