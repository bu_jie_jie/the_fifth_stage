package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.WorkDao;
import com.cn.wanxi.dao.impl.WorkImpl;
import com.cn.wanxi.model.WorkModel;
import com.cn.wanxi.service.IWorkService;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class WorkServiceImpl implements IWorkService {
    @Override
    public List<WorkModel> getWorkList() {
        Jedis jedis=new Jedis();
        List<WorkModel> list=new ArrayList<>();
        long length=jedis.llen("workId");
        if (length>0){
            List<String> workImgHref=jedis.lrange("workImgHref",0,length);
            List<String> workPictureHref=jedis.lrange("workPictureHref",0,length);
            List<String> workContentMan=jedis.lrange("workContentMan",0,length);
            List<String> workContentWoman=jedis.lrange("workContentWoman",0,length);
            for (int i = 0;i<length;i++){
                WorkModel workModel=new WorkModel();
                workModel.setImgHref(workImgHref.get(i));
                workModel.setPictureHref(workPictureHref.get(i));
                workModel.setContentMan(workContentMan.get(i));
                workModel.setContentWoman(workContentWoman.get(i));
                list.add(workModel);
            }
        }else {
            WorkDao workDao=new WorkImpl();
            list=workDao.getNavModelForFront();
            for (WorkModel model: list) {
                jedis.rpush("workId", String.valueOf(model.getId()));
                jedis.rpush("workImgHref",model.getImgHref());
                jedis.rpush("workPictureHref",model.getPictureHref());
                jedis.rpush("workContentMan",model.getContentMan());
                jedis.rpush("workContentWoman",model.getContentWoman());
            }
        }
        return list ;
    }
}
