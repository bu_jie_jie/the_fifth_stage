package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.ClothingDao;
import com.cn.wanxi.dao.impl.ClothingImpl;
import com.cn.wanxi.model.ClothingModel;
import com.cn.wanxi.service.ICothingService;

import java.util.List;

public class CothingServiceImpl implements ICothingService {
    /**
     * 查询当前服装类是否存在
     * @param clothingModel
     * @return
     */
    @Override
    public int add(ClothingModel clothingModel) {
        int result=0;
        if (findByName(clothingModel.getName())){
            result=-2;
        }else {
            ClothingDao clothingDao=new ClothingImpl();
            result=clothingDao.add(clothingModel);
        }
        return result;
    }

    @Override
    public List<ClothingModel> findAll() {
        ClothingDao clothingDao=new ClothingImpl();
        return clothingDao.findAll();
    }

    @Override
    public int del(int id) {
        ClothingDao clothingDao=new ClothingImpl();
        return clothingDao.del(id);
    }

    @Override
    public ClothingModel findById(int i) {
        ClothingDao clothingDao=new ClothingImpl();
        return clothingDao.findById(i);
    }

    @Override
    public int edit(ClothingModel clothingModel) {
        ClothingDao clothingDao=new ClothingImpl();
        return clothingDao.edit(clothingModel);
    }

    private boolean findByName(String name){
        ClothingDao clothingDao=new ClothingImpl();
        return clothingDao.findByName(name);
    }
}
