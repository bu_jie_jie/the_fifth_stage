package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.CustomizeDao;
import com.cn.wanxi.dao.impl.CustomizeImpl;
import com.cn.wanxi.model.CustomizeModel;
import com.cn.wanxi.service.ICustomizeService;

import java.util.List;

public class CustomizeServiceImpl implements ICustomizeService {
    @Override
    public int add(CustomizeModel customizeModel) {
        if (findByTitle(customizeModel.getTitle())){
            return -2;
        }else {
            CustomizeDao customizeDao=new CustomizeImpl();
            return customizeDao.add(customizeModel);
        }

    }

    @Override
    public List<CustomizeModel> findAll() {
        CustomizeDao customizeDao=new CustomizeImpl();
        return customizeDao.findAll();
    }

    @Override
    public int del(int id) {
        CustomizeDao customizeDao=new CustomizeImpl();
        return customizeDao.del(id);
    }

    @Override
    public CustomizeModel findById(int id) {
        CustomizeDao customizeDao=new CustomizeImpl();
        return customizeDao.findById(id);
    }

    @Override
    public int edit(CustomizeModel customizeModel) {
        CustomizeDao customizeDao=new CustomizeImpl();
        return customizeDao.edit(customizeModel);
    }

    private boolean findByTitle(String title) {
        CustomizeDao customizeDao=new CustomizeImpl();
        return customizeDao.findByTitle(title);
    }
}
