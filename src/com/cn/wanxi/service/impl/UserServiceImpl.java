package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.IUserDao;
import com.cn.wanxi.dao.impl.UserImpl;
import com.cn.wanxi.model.UserModel;
import com.cn.wanxi.service.IUserService;

import java.util.List;

public class UserServiceImpl implements IUserService {
    //用户注册添加用户逻辑
    @Override
    public int add(UserModel userModel) {
        IUserDao iUserDao = new UserImpl();
        // 拿到用户名去数据库查询，如果存在则提示用户用户名存在，返回true，否则返回false
        int count = 0;
        if (findByUsername(userModel.getUsername())) {
            count = -1;
        }else {
            count=iUserDao.add(userModel);
        }
        return count;
    }
    //用户登录逻辑
    @Override
    public int login(UserModel userModel) {
        int result=0;
        //1.判断验证是否正确
        if (userModel.getCode1().equals(userModel.getCode())){
            //2.判断用户是否存在
            //3.判断用户密码是否正确
            if (findByUsernameAndPassword(userModel)) {
                 result=1;
            }
        }else {
             result=-1;
        }
        return result;
    }

    @Override
    public List<UserModel> findAll(UserModel userModel) {
        IUserDao iUserDao=new UserImpl();
        return iUserDao.findAll(userModel);
    }

    @Override
    public int del(int id) {
        IUserDao iUserDao=new UserImpl();
        return iUserDao.del(id);
    }

    @Override
    public UserModel findById(int id) {
        IUserDao iUserDao=new UserImpl();
        return iUserDao.findById(id);
    }

    @Override
    public int edit(UserModel userModel) {
        IUserDao iUserDao=new UserImpl();
        return iUserDao.edit(userModel);
    }

    @Override
    public int getCount(UserModel userModel) {
        IUserDao iUserDao=new UserImpl();
        return iUserDao.getCount(userModel);
    }

    /**
     * 根据用户名和密码查询，如果存在则返回true，不存在返回false
     * @param userModel
     * @return
     */
    private boolean findByUsernameAndPassword(UserModel userModel) {
        IUserDao iUserDao=new UserImpl();
        return iUserDao.findByUsernameAndPassword(userModel);
    }


    private boolean findByUsername(String username){
        IUserDao iUserDao=new UserImpl();
        return iUserDao.findByUsername(username);
    }


}
