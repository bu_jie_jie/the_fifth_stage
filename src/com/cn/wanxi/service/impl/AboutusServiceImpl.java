package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.AboutusDao;
import com.cn.wanxi.dao.impl.AboutusImpl;
import com.cn.wanxi.model.AboutusModel;
import com.cn.wanxi.service.IAboutusService;
import redis.clients.jedis.Jedis;

import java.util.List;

public class AboutusServiceImpl implements IAboutusService {
    @Override
    public int add(AboutusModel aboutusModel) {
        if (findByTitle(aboutusModel.getTitle())) {
            return -2;
        } else {
            AboutusDao aboutusDao = new AboutusImpl();
            return aboutusDao.add(aboutusModel);
        }
    }

    @Override
    public List<AboutusModel> findAll() {
        AboutusDao aboutusDao = new AboutusImpl();
        return aboutusDao.findAll();
    }

    @Override
    public int del(int id) {
        AboutusDao aboutusDao = new AboutusImpl();
        return aboutusDao.del(id);
    }

    @Override
    public AboutusModel findById(int id) {
        AboutusDao aboutusDao = new AboutusImpl();
        return aboutusDao.findById(id);
    }

    @Override
    public int edit(AboutusModel aboutusModel) {
        AboutusDao aboutusDao = new AboutusImpl();
        return aboutusDao.edit(aboutusModel);
    }

    @Override
    public AboutusModel getAboutusModel() {
        AboutusModel aboutusModel = null;
        Jedis jedis = new Jedis();
        //首先判断缓存是否有值
        if (jedis.exists("aboutusId")) {
            aboutusModel=new AboutusModel();
            aboutusModel.setImgHref(jedis.get("aboutusImg_href"));
            aboutusModel.setContents(jedis.get("aboutusContents"));
        } else {
            AboutusDao aboutusDao = new AboutusImpl();
            aboutusModel = aboutusDao.getAboutusModelForFront();
            //先将id 存入缓存 再将 缓存清空
            jedis.setex("aboutusId",60*30,aboutusModel.getId().toString());
            jedis.set("aboutusImg_href",aboutusModel.getImgHref());
            jedis.set("aboutusContents",aboutusModel.getContents());
        }
        return aboutusModel;
    }

    private boolean findByTitle(String title) {
        AboutusDao aboutusDao = new AboutusImpl();
        return aboutusDao.findByTitle(title);
    }
}
