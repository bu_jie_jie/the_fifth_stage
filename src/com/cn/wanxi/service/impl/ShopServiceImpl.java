package com.cn.wanxi.service.impl;

import com.cn.wanxi.dao.ShopDao;
import com.cn.wanxi.dao.impl.ShopImpl;
import com.cn.wanxi.model.ShopModel;
import com.cn.wanxi.service.IShopService;

import java.util.List;

public class ShopServiceImpl implements IShopService {
    @Override
    public int add(ShopModel shopModel) {
        if (findByName(shopModel.getName())){
            return -2;
        }else {
            ShopDao shopDao=new ShopImpl();
            return shopDao.add(shopModel);
        }
    }

    public List<ShopModel> findAll() {
        ShopDao shopDao=new ShopImpl();
        return shopDao.findAll();
    }


    public int del(int id) {
        ShopDao shopDao=new ShopImpl();
        return shopDao.del(id);
    }


    public ShopModel findById(int id) {
        ShopDao shopDao=new ShopImpl();
        return shopDao.findById(id);
    }


    public int edit(ShopModel shopModel) {
        ShopDao shopDao=new ShopImpl();
        return shopDao.edit(shopModel);
    }
    private boolean findByName(String name) {
        ShopDao shopDao=new ShopImpl();
        return shopDao.findByName(name);
    }
}
