package com.cn.wanxi.service;

import com.cn.wanxi.model.ProductModel;

import java.util.List;

public interface IProductService {
    int add(ProductModel productModel);
    List<ProductModel> findAll();

    int del(int id);

    ProductModel findById(int i);

    int update(ProductModel productModel);
}
