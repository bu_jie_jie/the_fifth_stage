package com.cn.wanxi.service;

import com.cn.wanxi.model.ClothingModel;

import java.util.List;

public interface ICothingService {
    int add(ClothingModel clothingModel);
    List<ClothingModel> findAll();

    int del(int id);

    ClothingModel findById(int i);

    int edit(ClothingModel clothingModel);
}
