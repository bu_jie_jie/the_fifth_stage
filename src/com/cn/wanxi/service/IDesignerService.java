package com.cn.wanxi.service;

import com.cn.wanxi.model.DesignerModel;

import java.util.List;

public interface IDesignerService {
    int add(DesignerModel designerModel);

    int del(int i);

    int edit(DesignerModel designerModel);

    List<DesignerModel> findAll();

    DesignerModel findById(int i);
}
