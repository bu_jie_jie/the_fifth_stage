package com.cn.wanxi.service;

import com.cn.wanxi.model.CompanyModel;

import java.util.List;

public interface ICompanyService {
    int add(CompanyModel companyModel);

    List<CompanyModel> findAll();

    int del(int id);

    CompanyModel findById(int id);

    int edit(CompanyModel companyModel);

    CompanyModel getCompanyModel();
}
