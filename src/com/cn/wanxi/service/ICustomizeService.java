package com.cn.wanxi.service;

import com.cn.wanxi.model.CustomizeModel;

import java.util.List;

public interface ICustomizeService {
    int add(CustomizeModel customizeModel);

    List<CustomizeModel> findAll();

    int del(int i);

    CustomizeModel findById(int id);

    int edit(CustomizeModel customizeModel);
}
