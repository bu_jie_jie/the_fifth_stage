package com.cn.wanxi.service;

import com.cn.wanxi.model.NavModel;

import java.util.List;

public interface INavService {
    int add(NavModel navModel);

    List<NavModel> findAll();

    int del(int id);


    NavModel findByid(Integer id);

    int edit(NavModel navModel);


    List<NavModel> getNavList();
}
