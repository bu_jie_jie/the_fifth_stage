package com.cn.wanxi.service;

import com.cn.wanxi.model.AboutusModel;

import java.util.List;

public interface IAboutusService {
    int add(AboutusModel aboutusModel);

    List<AboutusModel> findAll();

    int del(int i);

    AboutusModel findById(int id);

    int edit(AboutusModel customizeModel);

    AboutusModel getAboutusModel();
}
