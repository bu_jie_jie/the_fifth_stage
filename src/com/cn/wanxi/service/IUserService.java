package com.cn.wanxi.service;

import com.cn.wanxi.model.UserModel;

import java.util.List;

public interface IUserService {
    //用户注册==添加用户
    int add(UserModel userModel);
    //用户登录
    int login(UserModel userModel);

    List<UserModel> findAll(UserModel userModel);

    int del(int id);

    UserModel findById(int id);

    int edit(UserModel userModel);

    int getCount(UserModel userModel);
}
