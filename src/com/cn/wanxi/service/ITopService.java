package com.cn.wanxi.service;

import com.cn.wanxi.model.TopModel;

public interface ITopService {
    TopModel getTopModel();
}
