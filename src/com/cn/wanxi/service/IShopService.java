package com.cn.wanxi.service;

import com.cn.wanxi.model.ShopModel;

import java.util.List;

public interface IShopService {
    int add(ShopModel shopModel);

    int del(int i);

    int edit(ShopModel shopModel);

    List<ShopModel> findAll();

    ShopModel findById(int i);
}
