package com.cn.wanxi.service;

import com.cn.wanxi.model.NewsTitleModel;

import java.util.List;

public interface INewsTitleService {
    int add(NewsTitleModel newsTitleModel);

    List<NewsTitleModel> findAll();

    int del(int id);

    NewsTitleModel findById(int id);

    int edit(NewsTitleModel newsTitleModel);

    List<NewsTitleModel> getnewsTitleModels();
}
