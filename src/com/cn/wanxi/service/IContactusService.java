package com.cn.wanxi.service;

import com.cn.wanxi.model.ContactusModel;

import java.util.List;

public interface IContactusService {
    List<ContactusModel> findAll();
}
