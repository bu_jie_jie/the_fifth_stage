package com.cn.wanxi.frontServlet;

import com.cn.wanxi.model.*;
import com.cn.wanxi.service.*;
import com.cn.wanxi.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/index.jsp")
public class IndexServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //关于我们——首页顶部图片和文字
        ITopService iTopService=new TopServiceImpl();
        TopModel topModel=iTopService.getTopModel();
        req.setAttribute("topModel",topModel);

        IAboutusService iAboutusService=new AboutusServiceImpl();
        AboutusModel aboutusModel=iAboutusService.getAboutusModel();
        req.setAttribute("aboutusModel",aboutusModel);
        //导航栏
        INavService iNavService=new NavServiceImpl();
        List<NavModel> navList=iNavService.getNavList();
        req.setAttribute("navList",navList);
        //底部公司信息
        ICompanyService iCompanyService=new CompanyServiceImpl();
        CompanyModel companyModel=iCompanyService.getCompanyModel();
        req.setAttribute("companyModel",companyModel);
        //作品欣赏
        IWorkService iWorkService=new WorkServiceImpl();
        List<WorkModel> workList=iWorkService.getWorkList();
        req.setAttribute("workList",workList);

        INewsTitleService iNewsTitleService=new NewsTitleServiceImpl();
        List<NewsTitleModel> newsTitleModels=iNewsTitleService.getnewsTitleModels();
        req.setAttribute("newsTitleModels",newsTitleModels);
        //将数据返回给前端页面
        req.getRequestDispatcher("/jsp/index.jsp").forward(req,resp);
    }
}
