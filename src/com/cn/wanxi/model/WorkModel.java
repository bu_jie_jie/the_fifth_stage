package com.cn.wanxi.model;

public class WorkModel {
    private Integer id;
    private String imgHref;
    private String pictureHref;
    private String contentMan;
    private String contentWoman;
    private String creationTime;
    private String updateTime;
    private Integer isShow;
    private Integer isTop;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImgHref() {
        return imgHref;
    }

    public void setImgHref(String imgHref) {
        this.imgHref = imgHref;
    }

    public String getPictureHref() {
        return pictureHref;
    }

    public void setPictureHref(String pictureHref) {
        this.pictureHref = pictureHref;
    }

    public String getContentMan() {
        return contentMan;
    }

    public void setContentMan(String contentMan) {
        this.contentMan = contentMan;
    }

    public String getContentWoman() {
        return contentWoman;
    }

    public void setContentWoman(String contentWoman) {
        this.contentWoman = contentWoman;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsShow() {
        return isShow;
    }

    public void setIsShow(Integer isShow) {
        this.isShow = isShow;
    }

    public Integer getIsTop() {
        return isTop;
    }

    public void setIsTop(Integer isTop) {
        this.isTop = isTop;
    }
}
